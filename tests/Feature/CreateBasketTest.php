<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateBasketTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->withHeaders(['Content-Type' => 'application/json'])->json('POST', '/api/promo', [
            'cashbox_id' => 2,
            'basket' => [
                [
                    'product_id' => 99989,
                    'name' => 'Авент 1 Нейчерал Соска силиконовая средний поток с 3 мес №2',
                    'price' => 84.00,
                    'count' => 1,
                    'cost' => 83.00
                ],
                [
                    'product_id' => 90989,
                    'name' => 'Авент 2 Нейчерал Соска силиконовая средний поток с 3 мес №2',
                    'price' => 84.00,
                    'count' => 1,
                    'cost' => 83.00
                ],
                [
                    'product_id' => 91989,
                    'name' => 'Авент 3 Нейчерал Соска силиконовая средний поток с 3 мес №2',
                    'price' => 84.00,
                    'count' => 1,
                    'cost' => 83.00
                ], [
                    'product_id' => 92989,
                    'name' => 'Авент 4 Нейчерал Соска силиконовая средний поток с 3 мес №2',
                    'price' => 84.00,
                    'count' => 1,
                    'cost' => 83.00
                ]
            ],
            'basket_cost' => 83.00,
            'additional' => [
                [
                    'product_id' => 5,
                    'name' => 'Dirol резинка жевательная Морозная Мята 13,6г №10',
                    'price' => 765.00
                ],
                [
                    'product_id' => 392,
                    'name' => 'Иммуноглобулин человеческий для в/в применения 5% фл. 25мл N1',
                    'price' => 765.00
                ],
                [
                    'product_id' => 912,
                    'name' => 'КОДАЛИ Гель для душа и ванн 200 мл. арт.016',
                    'price' => 765.00
                ],
                [
                    'product_id' => 942,
                    'name' => 'КОДАЛИ Эмульсия Пульп Витамине от 1-ых морщин 40мл арт.019',
                    'price' => 786.00
                ],
                [
                    'product_id' => 929,
                    'name' => 'КОДАЛИ Винэксперт сыворотка укрепляющая 30мл',
                    'price' => 786.00
                ],
                [
                    'product_id' => 871,
                    'name' => 'Ессентуки-17 Минеральная вода 0,5л бутылка',
                    'price' => 765.00
                ],
            ]
        ]);

        //$response->dump();
        //$response->dumpHeaders();
        $response->assertStatus(200);
        $response->getContent();
    }
}
