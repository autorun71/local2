<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DemoTest extends TestCase
{
    const CASHBOX_ID = 2;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testDemo()
    {
        //$this->markTestSkipped('must be revisited.');
        $this->addAdditional();
        $basketCost = 123.56;
        for ($i = 1; $i <= 5; $i++) {
            $this->addBasket($i, $basketCost);
            $basketCost += 567.45;
            sleep(1);
        }

        //$this->clearBasket();
        $this->closeSession();
    }

    private function addAdditional()
    {
        $response = $this->withHeaders(['Content-Type' => 'application/json'])->json('POST', '/api/promo', [
            'cashbox_id' => self::CASHBOX_ID,
            'additional' => [
                [
                    'product_id' => 912,
                    'name' => 'КОДАЛИ Гель для душа и ванн 200 мл. арт.016',
                    'price' => 765.00,
                ],
                [
                    'product_id' => 942,
                    'name' => 'КОДАЛИ Эмульсия Пульп Витамине от 1-ых морщин 40мл арт.019',
                    'price' => 765.00,
                ],
                [
                    'product_id' => 929,
                    'name' => 'КОДАЛИ Винэксперт сыворотка укрепляющая 30мл',
                    'price' => 786.00,
                ],
                [
                    'product_id' => 5,
                    'name' => 'Dirol резинка жевательная Морозная Мята 13,6г №10',
                    'price' => 786.00,
                ],
                [
                    'product_id' => 392,
                    'name' => 'Иммуноглобулин человеческий для в/в применения 5% фл. 25мл N1',
                    'price' => 786.00,
                ],
                [
                    'product_id' => 871,
                    'name' => 'Ессентуки-17 Минеральная вода 0,5л бутылка',
                    'price' => 786.00,
                ],
                [
                    'product_id' => 872,
                    'name' => 'Ессентуки-4 Минеральная вода 0,5л бутылка',
                    'price' => 786.00,
                ],
                [
                    'product_id' => 662,
                    'name' => 'Полисорб МП пор 1г пак N10',
                    'price' => 786.00,
                ],
                [
                    'product_id' => 494,
                    'name' => 'Омник Окас таб. 0.4 мг. №30',
                    'price' => 786.00,
                ],
            ],
        ]);
        $response->assertStatus(200);
    }

    private function addBasket($length = 10, $basketCost = 123.45)
    {
        $basket = [
            [
                'product_id' => 362,
                'name' => 'А-Дерма Реальба Мыло без мыла с молочком овса дерматологическое 100г',
                'price' => 80.00,
                'count' => 2,
                'cost' => 160.00,
            ],
            [
                'product_id' => 362,
                'name' => 'А-Дерма Реальба Мыло без мыла с молочком овса дерматологическое 100г',
                'price' => 80.00,
                'count' => 3,
                'cost' => 160.00,
            ],
            [
                'product_id' => 572,
                'name' => 'Спеленок Сок яблоко зеленое обогащенный железом 0,2л',
                'price' => 70.00,
                'count' => 2,
                'cost' => 140.00,
            ],
            [
                'product_id' => 555,
                'name' => 'Флорацид таб. п.о. 250 мг. №5',
                'price' => 70.00,
                'count' => 2,
                'cost' => 140.00,
            ],
            [
                'product_id' => 393,
                'name' => 'Релаксан Гольфы компрессионные CottonSocks 140den мужские черные р.4 (820)',
                'price' => 70.00,
                'count' => 2,
                'cost' => 140.00,
            ],
            [
                'product_id' => 546,
                'name' => 'Пропафенон таб. п.о. 150 мг. №40',
                'price' => 70.00,
                'count' => 2,
                'cost' => 140.00,
            ],
            [
                'product_id' => 386,
                'name' => 'Пластырь Omnipor фиксир гипоаллергенный 5см х 5м №1',
                'price' => 70.00,
                'count' => 2,
                'cost' => 140.00,
            ],
            [
                'product_id' => 975,
                'name' => 'Лендацин пор. д/р-ра для в/в и в/м введ. 1000 мг. фл. №5',
                'price' => 70.00,
                'count' => 2,
                'cost' => 140.00,
            ],
            [
                'product_id' => 399,
                'name' => 'Линза контактная Biomedics 38 R=8,6  -0,75',
                'price' => 70.00,
                'count' => 2,
                'cost' => 140.00,
            ],
            [
                'product_id' => 484,
                'name' => 'Сальгим р-р д/ингал. 10 мл. фл. №10',
                'price' => 70.00,
                'count' => 2,
                'cost' => 140.00,
            ],
            [
                'product_id' => 338,
                'name' => 'Бандаж хирург. послеоперац. на брюшную стенку (3-х панельный, шир. 23 см.) AB-309 разм. S',
                'price' => 70.00,
                'count' => 2,
                'cost' => 140.00,
            ],
            [
                'product_id' => 544,
                'name' => 'Кораксан таб.п.п.о.5мг №56',
                'price' => 70.00,
                'count' => 2,
                'cost' => 140.00,
            ],
            [
                'product_id' => 558,
                'name' => 'Кестин таб.п.п.о.20мг №10',
                'price' => 70.00,
                'count' => 2,
                'cost' => 140.00,
            ],
            [
                'product_id' => 746,
                'name' => 'Отривин спрей наз.доз.0,1% фл.10мл',
                'price' => 70.00,
                'count' => 2,
                'cost' => 140.00,
            ],
            [
                'product_id' => 485,
                'name' => 'Жанин таб.п.о.2мг+0,03мг №63',
                'price' => 70.00,
                'count' => 2,
                'cost' => 140.00,
            ],
            [
                'product_id' => 858,
                'name' => 'Каталин табл. д/глазн капель + р-ль фл 15мл N1',
                'price' => 70.00,
                'count' => 2,
                'cost' => 140.00,
            ],
            [
                'product_id' => 549,
                'name' => 'Акридерм ГК крем д/наруж.прим.0,05%+0,1%+1% туба 30г',
                'price' => 70.00,
                'count' => 2,
                'cost' => 140.00,
            ],
            [
                'product_id' => 358,
                'name' => 'БейбиКалм сироп д/младенцев фл с дозатором 15мл №1',
                'price' => 70.00,
                'count' => 2,
                'cost' => 140.00,
            ],
            [
                'product_id' => 34,
                'name' => 'Мустела Бебе Гель д/купания с рождения фл.с помпой 500мл №1 арт.8301708',
                'price' => 70.00,
                'count' => 2,
                'cost' => 140.00,
            ],
            [
                'product_id' => 487,
                'name' => 'Аугментин таб.п.п.о.500мг+125мг №14',
                'price' => 70.00,
                'count' => 2,
                'cost' => 140.00,
            ],
            [
                'product_id' => 719,
                'name' => 'Нафтадерм 10% 35г',
                'price' => 70.00,
                'count' => 2,
                'cost' => 140.00,
            ],
            [
                'product_id' => 489,
                'name' => 'Итразол 100мг капс №6',
                'price' => 70.00,
                'count' => 2,
                'cost' => 140.00,
            ]
        ];

        $array = array_slice($basket, 0, $length);

        $response = $this->withHeaders(
            ['Content-Type' => 'application/json']
        )->json('POST', '/api/promo', [
            'cashbox_id' => self::CASHBOX_ID,
            'basket' => $array,
            'basket_cost' => $basketCost,
        ]);
        $response->assertStatus(200);
    }

    private function clearBasket()
    {
        $response = $this->get('/api/clear-basket/' . self::CASHBOX_ID);
        $response->assertStatus(200);
    }

    private function closeSession()
    {
        $response = $this->get('/api/promo/close-session/2');
        //$response->dumpHeaders();
        //$response->dump();
        $response->assertStatus(200);
    }
}
