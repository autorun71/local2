<?php

return [
    'host' => env('CACHE_REGISTER_HOST', '127.0.0.1'),
    'port' => env('CACHE_REGISTER_PORT', '51234'),
    'type' => env('CACHE_REGISTER_TYPE', 'linux'),
    'timeout' => env('CACHE_REGISTER_TIMEOUT', '3'),
    'username' => env('CACHE_REGISTER_USERNAME', 'null'),
    'password' => env('CACHE_REGISTER_PASSWORD', 'null'),
];
