@extends('layouts.promo_layout')
@section('content')
@if($axis == 'v')
<promo-tv base-url="{{ url('/') }}" cashbox-id="{{ $cashbox_id }}"></promo-tv>
@else
<promo-tv-horizontal base-url="{{ url('/') }}" cashbox-id="{{ $cashbox_id }}"></promo-tv-horizontal>
@endif
@endsection
