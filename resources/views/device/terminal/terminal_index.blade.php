@extends('device.terminal.terminal_layout')
@section('terminal_index')
    <div class="col-12 text-center position-absolute" id="sleep-message" style="top: 45%; z-index: 1; right: 0px; left: 0px;">
        <h1>Подождите, идёт печать талона</h1>
    </div>
    <div class="container opacity" id="main-content">
            <div class="row justify-content-center">
                <div class="row justify-content-center">
                    <div><img src="{{ asset('/storage/settings/August2019/AGLVF78Mwx58f2L2P2iO.png') }}" class="logo"/></div>
                    <div class="pharmacy-header pt-2">АПТЕКА &laquo;ОЗЕРКИ&raquo;</div>
                </div>
            </div>
            <div class="col-md-12 d-flex justify-content-center">
                <span class="please-header text-center text-md-left" style="font-family: Verdana;
                        font-size: 36px;
                        line-height: 44px;
                        padding-top: 19px;
                        padding-bottom: 27px;
                        text-align: center;
                color: #000000;">{{ setting('terminal.header') }}</span>
            </div>
        <div class="container">
            <div class="form-group">
                <form action="{!! action('TerminalController@returnUrl') !!}" method="POST" id="terminal-form">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <div class="pr-0 pr-lg-5">
                                <button type="submit" class="btn btn-danger btn-lg btn-block"
                                        id="terminalDefaultOrderButton" name="defaultOrderButton" value="default"
                                        style="background-color: #FF0C3E;!important;">
                                    ОБЩАЯ ОЧЕРЕДЬ
                                </button>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="pl-0 pl-lg-5">
                                <button type="submit" class="btn btn-danger btn-lg btn-block"
                                        id="terminalOnlineOrderButton"
                                        name="onlineOrderButton" value="online"
                                        style="background-color: #FF0C3E;!important;">ВЫДАЧА ЗАКАЗОВ

                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="container justify-content-center">
                <div class="col-md-12 d-flex justify-content-center" style="margin-bottom: 10px;
                margin-top: -20px;">
                    <span class="time-footer" style="margin-right:15px;"></span>
                    <span class="date-footer"></span>
                </div>
            <div class="container-fluid" style="padding-left: 0px; padding-right: 0px; padding-top: 20px;">
                <div class="row h-100">
                    <div class="col-md-12">
                            <div id="slider"></div>
                    </div>
                </div>
            </div>
            <div class="container-fluid" style="height: 260px; width: 1022px; margin-top: 30px;">
                <div class="row">
                    <div id="tizer" style="
                        width: 490px;
                        height: 56px;
                        font-family: Verdana;
                        font-size: 24px;
                        font-style: normal;
                        font-weight: normal;
                        font-size: 24px;
                        line-height: 28px;
                        text-align: center;
                        color: #000000;
                        margin-bottom: 15px;">Установите мобильное приложение АПТЕКИ "ОЗЕРКИ"
                    </div>
                    <div id="phone" style="
                        padding-left: 45px;
                        height: 56px;
                        font-family: Verdana;
                        font-size: 24px;
                        font-style: normal;
                        font-weight: normal;
                        font-size: 24px;
                        line-height: 28px;
                        text-align: center;
                        color: #000000;">Телефон информационно-справочной </br> службы +7 (499) 603-00-00
                    </div>
                </div>
                <div class="row" style="padding-left: 70px; padding-top: 31px; ">
                    <img src="{{ asset('/storage/images/footer/google_play.png') }}" alt="" style="
                        width: 202.4px !important;
                        height: 172.1px;
                        margin-top: 0px;
                        z-index: 2;
                    ">
                    <img src="{{ asset('/storage/images/footer/qr_code.png') }}" alt="" style="margin-left: -12px; z-index: 1; height: 168px;"
                    >
                    {{-- style="width: 142px !important;
                    height: 146px !important; --}}
                    <div id="social" style="margin-top: 50px; margin-left: 170px;">
                    <img src="{{ asset('/storage/images/footer/site.png') }}" alt="" style="margin-bottom: 15px;"><br>
                    <img src="{{ asset('/storage/images/footer/mail.png') }}" alt="" style="margin-bottom: 15px;"><br>
                    {{-- <img src="{{ asset('images/footer/vk.png') }}" alt="" style="margin-bottom: 15px;"><br>
                    <img src="{{ asset('images/footer/insta.png') }}" alt=""> --}}
                    </div>
                    <div id="social_text" style="margin-top: 45px; margin-left: 70px;">
                    <div id="site" style="top: 709px;
                        font-family: Verdana;
                        font-style: normal;
                        font-weight: normal;
                        font-size: 24px;
                        line-height: 28px;
                        color: #000000;
                        margin-bottom: 15px;
                        margin-left: -50px;
                        margin-top: 10px;
                        ">6030000.ru</div>
                    <div id="mail" style="width: 123px;
                        font-family: Verdana;
                        font-style: normal;
                        font-weight: normal;
                        font-size: 24px;
                        line-height: 28px;
                        color: #000000;
                        margin-bottom: 20px;
                        margin-left: -50px;">office@erkapharm.com</div>
                    {{-- <div id="vk" style="width: 123px;
                        font-family: Roboto;
                        font-style: normal;
                        font-weight: normal;
                        font-size: 24px;
                        line-height: 28px;
                        color: #000000;
                        margin-bottom: 20px;
                        margin-left: -50px;">vk.com/ozerkiapteka</div>
                    <div id="insta" style="width: 123px;
                        font-family: Roboto;
                        font-style: normal;
                        font-weight: normal;
                        font-size: 24px;
S                        line-height: 28px;
                        color: #000000;
                        margin-bottom: 20px;
                        margin-left: -50px;">instagram.com/ozerki_apteki</div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
@stop
