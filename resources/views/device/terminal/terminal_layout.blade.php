<!doctype html>
<html lang="ru" class="h-90">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}" />

    <title>Электронная очередь</title>
</head>
<body class="h-90 disable-copy" oncontextmenu="return false;" onmousedown='return false;' onselectstart="return false"    style="margin-top: 60px;">
<body>
<div class="container h-90" id="app">

    @yield('terminal_index')

</div>
<script src="{{ asset('/js/app.js') }}" type="text/javascript"></script>
<script>
        window.updateTime = function updateTime() {
//Remove sleep message and opacity
$("#main-content").removeClass('opacity');
$("#sleep-message").hide();
}

$(document).ready(function () {

    setInterval('updateTime()', 1000);
});
</script>
{{-- <script src="/js/clock.js" type="text/javascript"></script> --}}
<script src="{{ asset('/js/main.js') }}" type="text/javascript"></script>
</body>
</html>
