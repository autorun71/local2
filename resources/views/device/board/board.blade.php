@extends('device.board.board_layout')
@section('board')
    <div hidden class="container-fluid font-weight-bold h-100 position-absolute" id="call-message">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="blink display-1 align-middle call-message-text">
                <span id="client-num">А001</span>
                <span>ОКНО №</span>
                <span id="cashbox-num">2</span>
            </div>
        </div>
    </div>
    <div class="container-fluid h-100" id="main-content" style="padding-right: 20px; padding-left: 20px;">
        <table class="w-100 align-self-center" style="margin:20px;">
            <tr>
                <td class="align-top w-50 align-middle">
                    <div class="row container-fluid justify-content-center">
                        <img src="{{ asset('/storage/settings/August2019/AGLVF78Mwx58f2L2P2iO.png') }}" class="logo" alt="">
                        <div class="pharmacy-header">АПТЕКА &laquo;ОЗЕРКИ&raquo;</div>
                    </div>
                </td>
                <td class="align-top w-50 align-middle">
                    <div class="pharmacy-header">ЭЛЕКТРОННАЯ ОЧЕРЕДЬ</div>
                </td>
            </tr>
        </table>
        <table class="w-100 align-self-center main-content">
            <tr>
                <td class="align-top w-50">
                        <div id="board_slider"></div>
                    {{-- <img src="/storage/{{ setting('tablo.banner') }}" class="banner-img"/> --}}
                </td>
                <td class="align-top w-50">
                    <table id="orderBoardTable" class="table-sm table-striped w-100">
                        <thead>
                        <tr class="board-header">
                            <th style="width: 40%">КЛИЕНТ</th>
                            <th style="width: 20%"></th>
                            <th style="width: 40%">ОКНО</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr id="order-row-0" class="order-table-row">
                            <td id="client-num" class="client-num-text" style="font-size: 80px;;"></td>
                            <td id="arrow"></td>
                            <td id="cashbox-num" class="cashbox-num-text" style="font-size: 80px;;"></td>
                        </tr>
                        <tr id="order-row-1" class="order-table-row">
                                <td id="client-num" class="client-num-text" style="font-size: 80px;;"></td>
                                <td id="arrow"></td>
                                <td id="cashbox-num" class="cashbox-num-text" style="font-size: 80px;;"></td>
                        </tr>
                        <tr id="order-row-2" class="order-table-row">
                                <td id="client-num" class="client-num-text" style="font-size: 80px;;"></td>
                                <td id="arrow"></td>
                                <td id="cashbox-num" class="cashbox-num-text" style="font-size: 80px;;"></td>
                        </tr>
                        <tr id="order-row-3" class="order-table-row">
                                <td id="client-num" class="client-num-text" style="font-size: 80px;;"></td>
                                <td id="arrow"></td>
                                <td id="cashbox-num" class="cashbox-num-text" style="font-size: 80px;;"></td>
                        </tr>
                        <tr id="order-row-4" class="order-table-row">
                                <td id="client-num" class="client-num-text" style="font-size: 80px;;"></td>
                                <td id="arrow"></td>
                                <td id="cashbox-num" class="cashbox-num-text" style="font-size: 80px;;"></td>
                        </tr>
                        <tr id="order-row-5" class="order-table-row">
                                <td id="client-num" class="client-num-text" style="font-size: 80px;;"></td>
                                <td id="arrow"></td>
                                <td id="cashbox-num" class="cashbox-num-text" style="font-size: 80px;;"></td>
                        </tr>
                        <tr id="order-row-6" class="order-table-row">
                                <td id="client-num" class="client-num-text" style="font-size: 80px;;"></td>
                                <td id="arrow"></td>
                                <td id="cashbox-num" class="cashbox-num-text" style="font-size: 80px;;"></td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
    </div>
@stop
