<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <script src="{{ asset('js/promo/app.js') }}" defer></script>
    <!--<script src="{{ asset('js/promo/app-react.js') }}" defer></script>-->
    <link href="{{ asset('css/promo/app.css') }}" rel="stylesheet">
    <style type="text/css">
        @font-face {
            font-family: 'Muller';
            src: local('Muller Regular'),
            local('Muller-Regular'),
            url('{{ url('/fonts/MullerRegular.woff2') }}') format('woff2'),
            url('{{ url('/fonts/MullerRegular.woff') }}') format('woff'),
            url('{{ url('/fonts/MullerRegular.ttf') }}') format('truetype');
            font-weight: 400;
            font-style: normal;
        }

        @font-face {
            font-family: 'Muller';
            src: local('Muller Bold'),
            local('Muller-Bold'),
            url('{{ url('/fonts/MullerBold.woff2') }}') format('woff2'),
            url('{{ url('/fonts/MullerBold.woff') }}') format('woff'),
            url('{{ url('/fonts/MullerBold.ttf') }}') format('truetype');
            font-weight: 700;
            font-style: normal;
        }

        @font-face {
            font-family: 'Circe';
            src: local('Circe Regular'),
            local('Circe-Regular'),
            url('{{ url('/fonts/Circe-Regular.woff2') }}') format('woff2'),
            url('{{ url('/fonts/Circe-Regular.woff') }}') format('woff'),
            url('{{ url('/fonts/Circe-Regular.ttf') }}') format('truetype');
            font-weight: 400;
            font-style: normal;
        }

        @font-face {
            font-family: 'Circe';
            src: local('Circe Bold'),
            local('Circe-Bold'),
            url('{{ url('/fonts/Circe-Bold.woff2') }}') format('woff2'),
            url('{{ url('/fonts/Circe-Bold.woff') }}') format('woff'),
            url('{{ url('/fonts/Circe-Bold.ttf') }}') format('truetype');
            font-weight: 700;
            font-style: normal;
        }
    </style>
</head>

<body>
    <div id="app">
        @yield('content')
    </div>
</body>

</html>
