@extends('layout')
@section('cashboxes')

    <div class="container">
        <div class="row">

            <!-- Cashbox 1 -->
            <div class="col-sm-6">
                <div class="container">
                    <div class="row">
                        <div class="col-sm h3">Касса №1
                            <div class="h6">Основная очередь</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm">
                            <button type="button" class="btn btn-outline-success btn-lg-cashbox btn-block cashBoxButton"
                                    data-url="/api/cashbox/set"
                                    data-number="1"
                                    data-type="default"
                                    data-modal="Табло кассы №1"
                                    data-toggle="modal"
                                    data-target="#modalCashBox" id="setClientCashBox1Button_">
                                Установить номер табло 1
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm">
                            <button type="button" class="btn btn-outline-success btn-lg-cashbox btn-block cashBoxButton"
                                    data-url="/api/cashbox/next"
                                    data-number="1"
                                    data-type="default"
                                    data-modal="Табло кассы №1"
                                    data-toggle="modal"
                                    data-target="#modalCashBox" id="nextClientCashBox1Button_">
                                Следующий клиент
                            </button>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-sm">
                            <button type="button" class="btn btn-outline-warning btn-lg-cashbox btn-block cashBoxButton"
                                    data-url="/api/cashbox/repeat"
                                    data-number="1"
                                    data-type="default"
                                    data-modal="Табло кассы №1"
                                    data-toggle="modal"
                                    data-target="#modalCashBox" id="repeatClientCashBox1Button_">
                                Вызвать следующего клиента повторно
                            </button>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-sm">
                            <button type="button" class="btn btn-outline-danger btn-lg-cashbox btn-block cashBoxButton"
                                    data-url="/api/cashbox/stop"
                                    data-number="1"
                                    data-type="default"
                                    data-modal="Табло кассы №1"
                                    data-toggle="modal"
                                    data-target="#modalCashBox" id="stopClientCashBox1Button_">
                                Завершить обслуживание клиента
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Cashbox 2 -->
            <div class="col-sm-6">
                <div class="container">
                    <div class="row">
                        <div class="col-sm h3">Касса №2
                            <div class="h6">Интернет-заказ</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm">
                            <button type="button" class="btn btn-outline-success btn-lg-cashbox btn-block cashBoxButton"
                                    data-url="/api/cashbox/set"
                                    data-number="2"
                                    data-type="online"
                                    data-modal="Табло кассы №2"
                                    data-toggle="modal"
                                    data-target="#modalCashBox" id="setClientCashBox1Button_">
                                Установить номер табло 2
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm">
                            <button type="button" class="btn btn-outline-success btn-lg-cashbox btn-block cashBoxButton"
                                    data-url="/api/cashbox/next"
                                    data-number="2"
                                    data-type="online"
                                    data-modal="Табло кассы №2"
                                    data-toggle="modal"
                                    data-target="#modalCashBox" id="nextClientCashBox2Button_">
                                Следующий клиент
                            </button>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-sm">
                            <button type="button" class="btn btn-outline-warning btn-lg-cashbox btn-block cashBoxButton"
                                    data-url="/api/cashbox/repeat"
                                    data-number="2"
                                    data-type="online"
                                    data-modal="Табло кассы №2"
                                    data-toggle="modal"
                                    data-target="#modalCashBox" id="repeatClientCashBox2Button_">
                                Вызвать следующего клиента повторно
                            </button>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-sm">
                            <button type="button" class="btn btn-outline-danger btn-lg-cashbox btn-block cashBoxButton"
                                    data-url="/api/cashbox/stop"
                                    data-number="2"
                                    data-type="online"
                                    data-modal="Табло кассы №2"
                                    data-toggle="modal"
                                    data-target="#modalCashBox" id="stopClientCashBox2Button_">
                                Завершить обслуживание клиента
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-5">

            <!-- Cashbox 3 -->
            <div class="col-sm">
                <div class="container">
                    <div class="row">
                        <div class="col-sm h3">Касса №3
                            <div class="h6">С возможностью выбора очереди</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm">
                            <button type="button" class="btn btn-outline-success btn-lg-cashbox btn-block cashBoxButton"
                                    data-url="/api/cashbox/set"
                                    data-number="3"
                                    data-type="default"
                                    data-modal="Табло кассы №3"
                                    data-toggle="modal"
                                    data-target="#modalCashBox" id="setClientCashBox3Button_">
                                Установить номер табло 3
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-outline-success btn-lg-cashbox cashBoxButton"
                                        data-url="/api/cashbox/next"
                                        data-number="3"
                                        data-type="default"
                                        data-modal="Табло кассы №3"
                                        data-toggle="modal"
                                        data-target="#modalCashBox" id="nextClientDefaultCashBox3Button_">
                                    Следующий клиент - Основная очередь
                                </button>
                                <button type="button" class="btn btn-outline-success btn-lg-cashbox cashBoxButton"
                                        data-url="/api/cashbox/next"
                                        data-number="3"
                                        data-type="online"
                                        data-modal="Табло кассы №3"
                                        data-toggle="modal"
                                        data-target="#modalCashBox" id="nextClientOnlineCashBox3Button_">
                                    Следующий клиент - Интернет-заказ
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-sm">
                            <button type="button" class="btn btn-outline-warning btn-lg-cashbox btn-block cashBoxButton"
                                    data-url="/api/cashbox/repeat"
                                    data-number="3"
                                    data-type="default"
                                    data-modal="Табло кассы №3"
                                    data-toggle="modal"
                                    data-target="#modalCashBox" id="repeatClientCashBox3Button_">
                                Вызвать следующего клиента повторно
                            </button>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-sm">
                            <button type="button" class="btn btn-outline-danger btn-lg-cashbox btn-block cashBoxButton"
                                    data-url="/api/cashbox/stop"
                                    data-number="3"
                                    data-type="default"
                                    data-modal="Табло кассы №3"
                                    data-toggle="modal"
                                    data-target="#modalCashBox" id="stopClientCashBox3Button_">
                                Завершить обслуживание клиента
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Cashbox 4 -->
            <div class="col-sm-6">
                <div class="container">
                    <div class="row">
                        <div class="col-sm h3">Касса №4
                            <div class="h6">Автоматический выбор из двух очередей по времени ожидания</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm">
                            <button type="button" class="btn btn-outline-success btn-lg-cashbox btn-block cashBoxButton"
                                    data-url="/api/cashbox/set"
                                    data-number="4"
                                    data-type="default"
                                    data-modal="Табло кассы №4"
                                    data-toggle="modal"
                                    data-target="#modalCashBox" id="setClientCashBox1Button">
                                Установить номер табло 4
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm">
                            <button type="button" class="btn btn-outline-success btn-lg-cashbox btn-block cashBoxButton"
                                    data-url="/api/cashbox/next"
                                    data-number="4"
                                    data-type="default"
                                    data-modal="Табло кассы №4"
                                    data-toggle="modal"
                                    data-target="#modalCashBox" id="nextClientCashBox4Button_">
                                Следующий клиент
                            </button>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-sm">
                            <button type="button" class="btn btn-outline-warning btn-lg-cashbox btn-block cashBoxButton"
                                    data-url="/api/cashbox/repeat"
                                    data-number="4"
                                    data-type="default"
                                    data-modal="Табло кассы №4"
                                    data-toggle="modal"
                                    data-target="#modalCashBox" id="repeatClientCashBox4Button_">
                                Вызвать следующего клиента повторно
                            </button>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-sm">
                            <button type="button" class="btn btn-outline-danger btn-lg-cashbox btn-block cashBoxButton"
                                    data-url="/api/cashbox/stop"
                                    data-number="4"
                                    data-type="default"
                                    data-modal="Табло кассы №4"
                                    data-toggle="modal"
                                    data-target="#modalCashBox" id="stopClientCashBox4Button_">
                                Завершить обслуживание клиента
                            </button>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>
    <div class="modal fade" id="modalCashBox" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Табло кассы</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="message-cashbox"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Закрыть
                    </button>
                </div>
            </div>
        </div>
    </div>
@stop
