import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

class Slider extends Component {
    render() {
        return (<Carousel autoPlay infiniteLoop showArrows={false} showStatus={false} showIndicators={false} showThumbs={false} dynamicHeight={false} stopOnHover={false} swipeable={false}>
            <div style={{ background: "white" }} className="h-100">
                <img src="../images/stoletov/promo.png" />
            </div>
            <div style={{ background: "white" }} className="h-100">
                <img src="../images/product/20-494.png" />
            </div>
            <div style={{ background: "white" }} className="h-100">
                <img src="../images/product/38-975.jpg" />
            </div>
            <div style={{ background: "white" }} className="h-100">
                <img src="../images/product/53-662.png" />
            </div>
        </Carousel>
        );
    }
}

ReactDOM.render(<Slider />, document.querySelector('.slider'));

                                            // Don't forget to include the css in your page

                                            // Using webpack
                                            // import styles from 'react-responsive-carousel/lib/styles/carousel.min.css';

                                            // Using html tag:
// <link rel="stylesheet" href="<NODE_MODULES_FOLDER>/react-responsive-carousel/lib/styles/carousel.min.css" />
