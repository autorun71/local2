import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

class BoardSliderCarousel  extends Component {
    render() {
        return (
            <Carousel
            infiniteLoop
            autoPlay
            showArrows={true}
            showIndicators={true}
            showThumbs={false}
            interval={6000}
            stopOnHover={false}
            width="100%"
            centerMode={false}
            showStatus={false} >
                <div>
                    <img src="\storage\images\slider_for_board\slider1.jpg "/>
                </div>
                <div>
                    <img src="\storage\images\slider_for_board\slider2.jpg "/>
                </div>
            </Carousel>
        );
    }
};

ReactDOM.render(
    <BoardSliderCarousel />,
    document.getElementById('board_slider')
);
