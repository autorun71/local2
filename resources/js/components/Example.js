import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TerminalCarousel from './TerminalSliderComponent';

export default class Example extends Component {
    render() {
        return (
            <div className="container" style={{padding: 0}}>
            <TerminalCarousel />

            </div>
        );
    }
}
 if (document.getElementById('slider')) {
     ReactDOM.render(<Example />, document.getElementById('slider'));
 }
