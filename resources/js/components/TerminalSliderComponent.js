import React, { Component } from 'react';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
class TerminalCarousel extends Component {
    constructor(props) {
        super(props);
        this.state = {  data: [] };
    }
    componentDidMount() {
        fetch(`http://vacancy.test.erkapharm.ru/api/promotion/images/terminal/%D0%90%D0%A30001`)
        .then(response => response.json())
        .then(data => {this.setState({data: data})
        })
    }
    render() {
        return (
            <Carousel
            infiniteLoop
            autoPlay
            showArrows={true}
            showIndicators={true}
            showThumbs={false}
            interval={6000}
            stopOnHover={false}
            centerMode={false}
            showStatus={false}>
            {this.state.data.map(function(item, i){
                return (
                    <img src={item.url} key={i}/>
                )
            })}
            </Carousel>
        );
    }
};
export default TerminalCarousel;
