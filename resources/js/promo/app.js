/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */
require('../bootstrap');
//require('../components/promo/slider/Slider');

window.Vue = require('vue');
/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//PromoTv
Vue.component('promo-tv', require('../components/promo/Tv.vue').default);
Vue.component('promo', require('../components/promo/Promo.vue').default);
Vue.component('promo-navbar', require('../components/promo/NavBar.vue').default);
Vue.component('promo-basket', require('../components/promo/Basket.vue').default);
Vue.component('product-card', require('../components/promo/ProductCard.vue').default);

Vue.component('promo-tv-horizontal', require('../components/promo/horizontal/TvHorizontal.vue').default);
Vue.component('promo-navbar-horizontal', require('../components/promo/horizontal/NavBar.vue').default);
Vue.component('promo-basket-h', require('../components/promo/horizontal/Basket.vue').default);

const app = new Vue({
    el: '#app',
});