/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */
require('./bootstrap');

window.Vue = require('vue');
/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
require('./components/Example');
//require('./components/BoardSliderComponent.js')

//require('./components/SliderKickStart.js')

//Vue
Vue.component('test-component', require('./components/TestComponent.vue').default);

//Dashboard
Vue.component('dashboard', require('./components/prototype/DashboardComponent.vue').default);

//Terminal prototype
Vue.component('terminal', require('./components/prototype/TerminalComponent.vue').default);
Vue.component('terminal-button', require('./components/prototype/TerminalButtonComponent.vue').default);

//Cashbox prototype
Vue.component('cashbox', require('./components/prototype/CashboxComponent.vue').default);

//Promotion prototype
Vue.component('promotion', require('./components/prototype/PromotionComponent.vue').default);

const app = new Vue({
    el: '#app',
});