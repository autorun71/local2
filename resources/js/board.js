//Max table lines
var maxLines = 7

//Set default delay flag for fix overlay
var delay = 0

$(document).ready(function () {

    //Get Token Id and set request message
    //var token_id = Cookies.get('token_id');
    //var dataToSend = {message: {"token_id": token_id, "order_type": "default"}};

    //Cashe media files
    var audioFiles = [
        //"/storage/sounds/bell.mp3",
        //"/storage/sounds/client.mp3",
        //"/storage/sounds/a.mp3",
        //"/storage/sounds/i.mp3",
        //"/storage/sounds/1.mp3",
        //"/storage/sounds/2.mp3",
        //"/storage/sounds/go.mp3"
    ]

    function preloadAudio(url) {

        var audio = new Audio()

        audio.addEventListener('canplaythrough', function () {
            console.log('Finish:' + url)
        }, false)
        audio.src = url
    }

    for (var i in audioFiles) {
        preloadAudio(audioFiles[i])
    }

    //Run Poll cycle
    (function update() {

        $.ajax({
            type: 'POST',
            url: "/api/board/active",
            //data: JSON.stringify(dataToSend),
            success: function (data) {
                var json = JSON.parse(data)

                var arrayLength = json.data.length

                if (arrayLength > maxLines) {
                    arrayLength = maxLines
                }

                for (i = 0; i < maxLines; i++) {

                    for (i = i; i < arrayLength; i++) {

                        $('#order-row-' + i + ' > #client-num').text(json.data[i].client_num);
                        $('#order-row-' + i + ' > #cashbox-num').text(json.data[i].cashbox_num);

                        //Call sound in reverse
                        if (json.data[arrayLength - 1 - i].sound_call == 1 && delay == 0) {
                            callClient(json.data[arrayLength - 1 - i].client_num, json.data[arrayLength - 1 - i].cashbox_num);
                            disableSound(json.data[arrayLength - 1 - i].id);
                        }
                    }

                    //Draw empty lines
                    $('#order-row-' + i + ' > #client-num').text("");
                    $('#order-row-' + i + ' > #cashbox-num').text("");
                }
            },
            error: function () {
                console.log('Error get active clients')
                setTimeout(function () {
                    window.location.reload(true);
                }, 5000);
            }
        }).then(function () {
            setTimeout(update, 2000);
        });
    })();
});

function play(audio) {
    var playPromise = audio.play();

    if (playPromise !== undefined) {
        playPromise.then(_ => {
            //console.log('Play audio');
        }).catch(error => {

            //Fix Promise exception in Chrome
            console.log('Promise exception');
            setTimeout(function () {
                window.location.reload(true);
            }, 5000);
        });
    }

    return new Promise(function (resolve, reject) {
        audio.addEventListener('ended', resolve);
    });
}

function callClient(clientNum, cashboxNum) {

    var fileExt = '.wav';

    //Format client num
    var orderLetter = clientNum.substr(0, 1), orderLetterEn = '';
    if (orderLetter === 'А') {
        orderLetterEn = 'a';
    } else if (orderLetter === 'И') {
        orderLetterEn = 'i';
    }
    var orderNum = clientNum.substr(1, 3);
    orderNum = parseInt(orderNum, 10);

    var audio1 = new Audio('/storage/sounds/bell' + fileExt);
    var audio2 = new Audio('/storage/sounds/client' + fileExt);
    var audio3 = new Audio('/storage/sounds/' + orderLetterEn + fileExt);

    /*
    if(orderNum > 20 && orderNum < 30) {
        var audio4 = new Audio('/storage/sounds/20' + fileExt);
        var fileNum = orderNum - 20;
        var audio5 = new Audio('/storage/sounds/' + fileNum + fileExt);
    } else if(orderNum > 30 && orderNum < 40) {
        var audio4 = new Audio('/storage/sounds/30' + fileExt);
        var fileNum = orderNum - 30;
        var audio5 = new Audio('/storage/sounds/' + fileNum + fileExt);
    } else if(orderNum > 40 && orderNum < 50) {
        var audio4 = new Audio('/storage/sounds/40' + fileExt);
        var fileNum = orderNum - 40;
        var audio5 = new Audio('/storage/sounds/' + fileNum + fileExt);
    } else if(orderNum > 50 && orderNum < 60) {
        var audio4 = new Audio('/storage/sounds/50' + $fileExt);
        var fileNum = orderNum - 50;
        var audio5 = new Audio('/storage/sounds/' + fileNum + fileExt);
    } else if(orderNum > 60 && orderNum < 70) {
        var audio4 = new Audio('/storage/sounds/60' + fileExt);
        var fileNum = orderNum - 60;
        var audio5 = new Audio('/storage/sounds/' + fileNum + fileExt);
    } else if(orderNum > 70 && orderNum < 80) {
        var audio4 = new Audio('/storage/sounds/70' + fileExt);
        var fileNum = orderNum - 70;
        var audio5 = new Audio('/storage/sounds/' + fileNum + fileExt);
    } else if(orderNum > 80 && orderNum < 90) {
        var audio4 = new Audio('/storage/sounds/80' + fileExt);
        var fileNum = orderNum - 80;
        var audio5 = new Audio('/storage/sounds/' + fileNum + fileExt);
    } else if(orderNum > 90 && orderNum < 100) {
        var audio4 = new Audio('/storage/sounds/90' + fileExt);
        var fileNum = orderNum - 90;
        var audio5 = new Audio('/storage/sounds/' + fileNum + fileExt);
    */
    if (orderNum > 100) {
        var digits = ("" + orderNum).split("");
        if (digits[1] == 0 && digits[2] == 0) {
            var audio4 = new Audio('/storage/sounds/' + 'place_' + digits[0] + '00' + fileExt);
        } else {
            var audio4 = new Audio('/storage/sounds/' + 'tick_' + digits[0] + '00' + '_' + fileExt);
            if (digits[1] == 0) {
                var audio5 = new Audio('/storage/sounds/' + 'place_' + digits[2] + fileExt);
            } else {
                var audio5 = new Audio('/storage/sounds/' + 'place_' + digits[1] + digits[2] + fileExt);
            }
        }
        /*
        if(digits[1] == 0) {
            var audio5 = new Audio('/storage/sounds/' + digits[2] + fileExt);
        } else if(digits[1] == 1) {
            var audio5 = new Audio('/storage/sounds/' + digits[1] + digits[2] + fileExt);
        } else {
            var audio5 = new Audio('/storage/sounds/' + digits[1] + '0' + fileExt);
            if(digits[2] !== "0") {
                var audio6 = new Audio('/storage/sounds/' + digits[2] + fileExt);
            }
        }
        */
    } else {
        var audio4 = new Audio('/storage/sounds/' + 'tick_' + orderNum + fileExt);
    }

    var audio7 = new Audio('/storage/sounds/go' + fileExt);
    var audio8 = new Audio('/storage/sounds/' + 'place_' + cashboxNum + fileExt);

    var nm = 123;
    var digits = ("" + nm).split("");
    console.log(digits[0]);
    console.log(digits[1]);
    console.log(digits[2]);

    $('#call-message').removeAttr('hidden');
    $('#client-num').text(clientNum);
    $('#cashbox-num').text(cashboxNum);
    $('#main-content').addClass('opacity-call');

    delay = 1;

    play(audio1).then(function () {
        if (typeof audio2 !== 'undefined') {
            return play(audio2);
        }
    }).then(function () {
        if (typeof audio3 !== 'undefined') {
            return play(audio3);
        }
    }).then(function () {
        if (typeof audio4 !== 'undefined') {
            return play(audio4);
        }
    }).then(function () {
        if (typeof audio5 !== 'undefined') {
            return play(audio5);
        }
    }).then(function () {
        if (typeof audio6 !== 'undefined') {
            return play(audio6);
        }
    }).then(function () {
        if (typeof audio7 !== 'undefined') {
            return play(audio7);
        }
    }).then(function () {
        if (typeof audio8 !== 'undefined') {
            return play(audio8);
        }
    }).then(function () {
        $('#main-content').removeClass('opacity-call');
        $('#call-message').attr('hidden', true);
        delay = 0;
    }).catch(error => {
        console.log('Error on play file')
        setTimeout(function () {
            window.location.reload(true);
        }, 5000);
    });
}

//Disable sound_call field in DB
function disableSound(id) {
    $.ajax({
        type: 'GET',
        url: "/api/board/active/disable/" + id,
        success: function (response) {
            console.log('Ok disable sound')
        },
        error: function () {
            console.log('Error disable sound')
            setTimeout(function () {
                window.location.reload(true);
            }, 5000);
        }
    });
}
