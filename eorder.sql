-- MariaDB dump 10.17  Distrib 10.4.7-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: eorder
-- ------------------------------------------------------
-- Server version	10.4.7-MariaDB-1:10.4.7+maria~stretch

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `active_orders`
--

DROP TABLE IF EXISTS `active_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `active_orders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_type_id` bigint(20) unsigned NOT NULL,
  `client_num` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sound_call` tinyint(1) NOT NULL,
  `cashbox_num` bigint(20) unsigned NOT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `print_ticket_time` timestamp NULL DEFAULT NULL,
  `active_time` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `active_orders_cashbox_num_unique` (`cashbox_num`),
  KEY `active_orders_order_type_id_foreign` (`order_type_id`),
  CONSTRAINT `active_orders_order_type_id_foreign` FOREIGN KEY (`order_type_id`) REFERENCES `order_types` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `active_orders`
--

LOCK TABLES `active_orders` WRITE;
/*!40000 ALTER TABLE `active_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `active_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_rows`
--

DROP TABLE IF EXISTS `data_rows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_rows`
--

LOCK TABLES `data_rows` WRITE;
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;
INSERT INTO `data_rows` VALUES (1,1,'id','number','ID',1,0,0,0,0,0,NULL,1),(2,1,'name','text','Name',1,1,1,1,1,1,NULL,2),(3,1,'email','text','Email',1,1,1,1,1,1,NULL,3),(4,1,'password','password','Password',1,0,0,1,1,0,NULL,4),(5,1,'remember_token','text','Remember Token',0,0,0,0,0,0,NULL,5),(6,1,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,6),(7,1,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,7),(8,1,'avatar','image','Avatar',0,1,1,1,1,1,NULL,8),(9,1,'user_belongsto_role_relationship','relationship','Role',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}',10),(10,1,'user_belongstomany_role_relationship','relationship','Roles',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}',11),(11,1,'settings','hidden','Settings',0,0,0,0,0,0,NULL,12),(12,2,'id','number','ID',1,0,0,0,0,0,NULL,1),(13,2,'name','text','Name',1,1,1,1,1,1,NULL,2),(14,2,'created_at','timestamp','Created At',0,0,0,0,0,0,NULL,3),(15,2,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,4),(16,3,'id','number','ID',1,0,0,0,0,0,NULL,1),(17,3,'name','text','Name',1,1,1,1,1,1,NULL,2),(18,3,'created_at','timestamp','Created At',0,0,0,0,0,0,NULL,3),(19,3,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,4),(20,3,'display_name','text','Display Name',1,1,1,1,1,1,NULL,5),(21,1,'role_id','text','Role',1,1,1,1,1,1,NULL,9),(22,4,'id','text','Id',1,0,0,0,0,0,'{}',1),(23,4,'name','text','Название',1,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\"}}',3),(24,4,'address','text','Адрес',0,1,1,1,1,1,'{}',5),(25,4,'comment','text','Комментарий',0,0,1,1,1,1,'{}',12),(26,4,'token_id','text','Token Id',1,1,1,1,1,1,'{\"validation\":{\"rule\":\"required|unique:pharmacies\"}}',8),(27,4,'pharmacy_network_id','text','Pharmacy Network Id',1,0,0,1,1,1,'{\"validation\":{\"rule\":\"required\"}}',2),(28,4,'created_at','timestamp','Время добавления',0,0,1,0,0,0,'{}',13),(29,4,'updated_at','timestamp','Время обновления',0,0,1,0,0,0,'{}',14),(30,4,'pharmacy_belongsto_pharmacy_network_relationship','relationship','Сеть аптек',0,1,1,1,1,1,'{\"model\":\"App\\\\PharmacyNetwork\",\"table\":\"pharmacy_networks\",\"type\":\"belongsTo\",\"column\":\"pharmacy_network_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"active_orders\",\"pivot\":\"0\",\"taggable\":\"0\"}',6),(31,5,'id','text','Id',1,0,0,0,0,0,'{}',1),(32,5,'name','text','Название',1,1,1,1,1,1,'{\"validation\":{\"rule\":\"required|unique:pharmacy_networks\"}}',2),(33,5,'logo','image','Логотип',0,1,1,1,1,1,'{}',4),(34,5,'comment','text','Комментарий',0,1,1,1,1,1,'{}',9),(35,5,'created_at','timestamp','Время добавления',0,0,1,0,0,0,'{}',6),(36,5,'updated_at','timestamp','Время обновления',0,0,1,0,0,0,'{}',10),(37,6,'id','text','Id',1,0,0,0,0,0,'{}',1),(38,6,'name','text','Название',1,1,1,1,1,1,'{\"validation\":{\"rule\":\"required|unique:order_types\"}}',2),(39,6,'alias','text','Короткое название',0,1,1,1,1,1,'{\"validation\":{\"rule\":\"required|unique:order_types\"}}',3),(40,6,'prefix','text','Префикс номера',1,1,1,1,1,1,'{}',4),(41,6,'comment','text','Комментарий',0,1,1,1,1,1,'{}',5),(42,6,'created_at','timestamp','Время добавления',0,0,1,0,0,0,'{}',6),(43,6,'updated_at','timestamp','Время обновления',0,0,1,0,0,0,'{}',7),(45,4,'pharmacy_belongstomany_order_type_relationship','relationship','Типы очередей',0,0,1,1,1,1,'{\"model\":\"App\\\\OrderType\",\"table\":\"order_types\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"pharmacy_order\",\"pivot\":\"1\",\"taggable\":\"0\"}',11),(46,7,'id','text','Id',1,0,0,0,0,0,'{}',1),(47,7,'pharmacy_id','text','Pharmacy Id',1,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\"}}',2),(49,7,'cashbox_num','number','Номер кассы',1,1,1,1,1,1,'{\"validation\":{\"rule\":\"required\"}}',4),(50,7,'token_id','text','Token Id',1,1,1,1,1,1,'{\"validation\":{\"rule\":\"required|unique:cashboxes\"}}',5),(51,7,'created_at','timestamp','Время добавления',0,0,1,0,0,0,'{}',6),(52,7,'updated_at','timestamp','Время обновления',0,0,1,0,0,0,'{}',7),(53,7,'cashbox_belongsto_pharmacy_relationship','relationship','Аптека',0,1,1,1,1,1,'{\"model\":\"App\\\\Pharmacy\",\"table\":\"pharmacies\",\"type\":\"belongsTo\",\"column\":\"pharmacy_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"active_orders\",\"pivot\":\"0\",\"taggable\":\"0\"}',8),(55,7,'cashbox_belongstomany_order_type_relationship','relationship','Типы очередей',0,1,1,1,1,1,'{\"model\":\"App\\\\OrderType\",\"table\":\"order_types\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"cashbox_order\",\"pivot\":\"1\",\"taggable\":\"0\"}',10),(56,4,'view_name','text','Видимое название',0,1,1,1,1,1,'{}',4),(57,5,'view_name','text','Видимое название',0,1,1,1,1,1,'{}',3),(58,4,'telephone','text','Телефон',0,1,1,1,1,1,'{}',7),(60,5,'telephone','text','Телефон',0,1,1,1,1,1,'{}',5),(62,5,'banner_terminal','image','Баннер терминал',0,1,1,1,1,1,'{}',7),(63,5,'banner_board','image','Баннер табло',0,1,1,1,1,1,'{}',8),(64,4,'banner_terminal','image','Баннер терминал',0,1,1,1,1,1,'{}',9),(65,4,'banner_board','image','Баннер табло',0,1,1,1,1,1,'{}',10);
/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_types`
--

DROP TABLE IF EXISTS `data_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_types`
--

LOCK TABLES `data_types` WRITE;
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
INSERT INTO `data_types` VALUES (1,'users','users','User','Users','voyager-person','TCG\\Voyager\\Models\\User','TCG\\Voyager\\Policies\\UserPolicy','TCG\\Voyager\\Http\\Controllers\\VoyagerUserController','',1,0,NULL,'2019-05-19 09:47:51','2019-05-19 09:47:51'),(2,'menus','menus','Menu','Menus','voyager-list','TCG\\Voyager\\Models\\Menu',NULL,'','',1,0,NULL,'2019-05-19 09:47:51','2019-05-19 09:47:51'),(3,'roles','roles','Role','Roles','voyager-lock','TCG\\Voyager\\Models\\Role',NULL,'','',1,0,NULL,'2019-05-19 09:47:51','2019-05-19 09:47:51'),(4,'pharmacies','pharmacies','Аптека','Аптеки',NULL,'App\\Pharmacy',NULL,NULL,NULL,1,0,'{\"order_column\":\"created_at\",\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}','2019-05-19 09:50:20','2019-05-28 14:57:41'),(5,'pharmacy_networks','pharmacy-networks','Сеть аптек','Сети аптек',NULL,'App\\PharmacyNetwork',NULL,NULL,NULL,1,0,'{\"order_column\":\"name\",\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}','2019-05-19 09:56:07','2019-05-26 12:06:18'),(6,'order_types','order-types','Очередь','Очереди',NULL,'App\\OrderType',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}','2019-05-19 10:03:09','2019-05-28 15:28:44'),(7,'cashboxes','cashboxes','Касса','Кассы',NULL,'App\\Cashbox',NULL,NULL,NULL,1,0,'{\"order_column\":\"updated_at\",\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}','2019-05-22 06:38:43','2019-05-22 18:08:36');
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_orders`
--

DROP TABLE IF EXISTS `log_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_orders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_type_id` bigint(20) unsigned NOT NULL,
  `client_num` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cashbox_num` tinyint(3) unsigned NOT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `print_ticket_time` timestamp NULL DEFAULT NULL,
  `active_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `log_orders_order_type_id_foreign` (`order_type_id`),
  CONSTRAINT `log_orders_order_type_id_foreign` FOREIGN KEY (`order_type_id`) REFERENCES `order_types` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_orders`
--

LOCK TABLES `log_orders` WRITE;
/*!40000 ALTER TABLE `log_orders` DISABLE KEYS */;
INSERT INTO `log_orders` VALUES (1,1,'A001',1,'2019-07-16 15:36:24',NULL,'2019-07-16 15:36:41','2019-07-16 15:38:19'),(2,2,'И001',2,'2019-07-16 15:36:28',NULL,'2019-07-16 15:36:43','2019-07-16 15:38:30'),(3,1,'A002',1,'2019-07-16 15:36:26',NULL,'2019-07-16 15:38:19','2019-07-16 15:39:15'),(4,2,'И002',2,'2019-07-16 15:36:30',NULL,'2019-07-16 15:38:30','2019-07-16 15:39:24'),(5,1,'A003',1,'2019-07-16 15:36:34',NULL,'2019-07-16 15:39:15','2019-07-16 15:39:46'),(6,2,'И003',2,'2019-07-16 15:36:32',NULL,'2019-07-16 15:39:24','2019-07-16 15:39:47'),(7,1,'A004',1,'2019-07-16 15:38:04',NULL,'2019-07-16 15:39:46','2019-07-16 15:39:49'),(8,1,'A005',1,'2019-07-16 15:38:07',NULL,'2019-07-16 15:39:49','2019-07-16 15:47:26'),(9,1,'A006',1,'2019-07-16 15:38:09',NULL,'2019-07-16 15:47:26','2019-07-16 15:57:35'),(10,2,'И004',2,'2019-07-16 15:38:05',NULL,'2019-07-16 15:39:48','2019-07-16 15:57:51'),(11,2,'И005',2,'2019-07-16 15:38:12',NULL,'2019-07-16 15:57:52','2019-07-16 15:57:54'),(12,2,'И006',2,'2019-07-16 15:38:14',NULL,'2019-07-16 15:57:54','2019-07-16 15:57:58'),(13,1,'A007',1,'2019-07-16 15:38:11',NULL,'2019-07-16 15:57:36','2019-07-16 15:58:00'),(14,2,'И007',2,'2019-07-16 15:39:37',NULL,'2019-07-16 15:57:58','2019-07-16 15:58:14'),(15,2,'И008',2,'2019-07-16 15:45:56',NULL,'2019-07-16 15:58:14','2019-07-16 16:33:26'),(16,2,'И009',1,'2019-07-16 15:46:40',NULL,'2019-07-16 16:23:09','2019-07-16 16:33:28'),(17,1,'A008',1,'2019-07-16 16:23:43',NULL,'2019-07-16 16:33:28','2019-07-16 16:33:40'),(18,2,'И010',2,'2019-07-16 15:46:42',NULL,'2019-07-16 16:33:27','2019-07-16 16:33:45'),(19,1,'A009',1,'2019-07-16 16:23:49',NULL,'2019-07-16 16:33:40','2019-07-16 16:34:12'),(20,2,'И011',2,'2019-07-16 16:23:45',NULL,'2019-07-16 16:33:45','2019-07-16 16:34:13'),(21,1,'A010',1,'2019-07-16 16:23:51',NULL,'2019-07-16 16:34:12','2019-07-16 16:34:31'),(22,2,'И012',2,'2019-07-16 16:23:47',NULL,'2019-07-16 16:34:13','2019-07-16 16:34:32'),(23,1,'A011',1,'2019-07-16 16:23:53',NULL,'2019-07-16 16:34:31','2019-07-16 16:34:34'),(24,2,'И013',2,'2019-07-16 16:23:57',NULL,'2019-07-16 16:34:33','2019-07-16 16:34:36'),(25,1,'A012',1,'2019-07-16 16:23:54',NULL,'2019-07-16 16:34:35','2019-07-16 16:35:00'),(26,2,'И014',2,'2019-07-16 16:23:58',NULL,'2019-07-16 16:34:36','2019-07-16 16:35:02'),(27,1,'A013',1,'2019-07-16 16:23:55',NULL,'2019-07-16 16:35:00','2019-07-16 16:38:32'),(28,2,'И015',2,'2019-07-16 16:33:14',NULL,'2019-07-16 16:35:02','2019-07-16 16:38:34'),(29,1,'A014',1,'2019-07-16 16:33:13',NULL,'2019-07-16 16:38:32','2019-07-16 16:38:56'),(30,1,'A015',1,'2019-07-16 16:33:16',NULL,'2019-07-16 16:38:56','2019-07-16 16:40:30'),(31,2,'И016',2,'2019-07-16 16:33:17',NULL,'2019-07-16 16:38:34','2019-07-16 16:40:32'),(32,1,'A016',1,'2019-07-16 16:34:06',NULL,'2019-07-16 16:40:30','2019-07-16 16:45:20'),(33,2,'И017',2,'2019-07-16 16:34:08',NULL,'2019-07-16 16:40:32','2019-07-16 16:45:22'),(34,1,'A017',1,'2019-07-16 16:34:09',NULL,'2019-07-16 16:45:20','2019-07-16 16:46:11'),(35,1,'A018',1,'2019-07-16 16:34:45',NULL,'2019-07-16 16:46:11','2019-07-16 16:46:13'),(36,1,'A019',1,'2019-07-16 16:34:46',NULL,'2019-07-16 16:46:13','2019-07-16 16:46:14'),(37,1,'A020',1,'2019-07-16 16:34:47',NULL,'2019-07-16 16:46:14','2019-07-16 16:46:15'),(38,1,'A001',1,'2019-07-16 16:34:48',NULL,'2019-07-16 16:46:15','2019-07-16 16:48:02'),(39,2,'И018',2,'2019-07-16 16:34:50',NULL,'2019-07-16 16:45:22','2019-07-16 16:48:04'),(40,2,'И019',2,'2019-07-16 16:34:51',NULL,'2019-07-16 16:48:04','2019-07-16 16:48:05'),(41,1,'A002',1,'2019-07-16 16:35:54',NULL,'2019-07-16 16:48:03','2019-07-16 16:48:07'),(42,2,'И020',2,'2019-07-16 16:34:52',NULL,'2019-07-16 16:48:05','2019-07-16 16:48:13'),(43,2,'И001',2,'2019-07-16 16:35:52',NULL,'2019-07-16 16:48:13','2019-07-16 16:48:15'),(44,1,'A003',1,'2019-07-16 16:52:31',NULL,'2019-07-16 16:52:36','2019-07-19 07:48:43'),(45,1,'A004',1,'2019-07-19 04:45:39',NULL,'2019-07-19 07:48:43','2019-07-19 07:48:47'),(46,2,'И002',2,'2019-07-16 16:52:33',NULL,'2019-07-16 16:52:37','2019-07-19 07:48:49'),(47,1,'A005',1,'2019-07-19 04:45:41',NULL,'2019-07-19 07:48:47','2019-07-19 09:19:51'),(48,2,'И003',2,'2019-07-19 05:35:10',NULL,'2019-07-19 07:48:49','2019-07-19 09:19:53'),(49,1,'A006',1,'2019-07-19 04:47:09',NULL,'2019-07-19 09:19:51','2019-08-14 06:53:26'),(50,1,'A001',1,'2019-08-15 04:43:48',NULL,'2019-08-15 04:43:57','2019-08-15 04:43:59'),(51,1,'A002',1,'2019-08-15 04:43:53',NULL,'2019-08-15 04:43:59','2019-08-15 04:44:00'),(52,2,'И001',2,'2019-08-15 04:43:51',NULL,'2019-08-15 04:44:01','2019-08-15 04:44:03');
/*!40000 ALTER TABLE `log_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_items`
--

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` VALUES (1,1,'Dashboard','','_self','voyager-boat',NULL,NULL,1,'2019-05-19 09:47:51','2019-05-19 09:47:51','voyager.dashboard',NULL),(2,1,'Media','','_self','voyager-images',NULL,NULL,5,'2019-05-19 09:47:51','2019-05-19 09:47:51','voyager.media.index',NULL),(3,1,'Users','','_self','voyager-person',NULL,NULL,3,'2019-05-19 09:47:51','2019-05-19 09:47:51','voyager.users.index',NULL),(4,1,'Roles','','_self','voyager-lock',NULL,NULL,2,'2019-05-19 09:47:51','2019-05-19 09:47:51','voyager.roles.index',NULL),(5,1,'Tools','','_self','voyager-tools',NULL,NULL,9,'2019-05-19 09:47:51','2019-05-19 09:47:51',NULL,NULL),(6,1,'Menu Builder','','_self','voyager-list',NULL,5,10,'2019-05-19 09:47:51','2019-05-19 09:47:51','voyager.menus.index',NULL),(7,1,'Database','','_self','voyager-data',NULL,5,11,'2019-05-19 09:47:51','2019-05-19 09:47:51','voyager.database.index',NULL),(8,1,'Compass','','_self','voyager-compass',NULL,5,12,'2019-05-19 09:47:51','2019-05-19 09:47:51','voyager.compass.index',NULL),(9,1,'BREAD','','_self','voyager-bread',NULL,5,13,'2019-05-19 09:47:51','2019-05-19 09:47:51','voyager.bread.index',NULL),(10,1,'Settings','','_self','voyager-settings',NULL,NULL,14,'2019-05-19 09:47:51','2019-05-19 09:47:51','voyager.settings.index',NULL),(11,1,'Hooks','','_self','voyager-hook',NULL,5,13,'2019-05-19 09:47:51','2019-05-19 09:47:51','voyager.hooks',NULL),(12,1,'Аптеки','','_self',NULL,NULL,NULL,15,'2019-05-19 09:50:20','2019-05-19 09:50:20','voyager.pharmacies.index',NULL),(13,1,'Сети аптек','','_self',NULL,NULL,NULL,16,'2019-05-19 09:56:07','2019-05-19 09:56:07','voyager.pharmacy-networks.index',NULL),(14,1,'Очереди','','_self',NULL,NULL,NULL,17,'2019-05-19 10:03:09','2019-05-19 10:03:09','voyager.order-types.index',NULL),(15,1,'Кассы','','_self',NULL,NULL,NULL,18,'2019-05-22 06:38:43','2019-05-22 06:38:43','voyager.cashboxes.index',NULL);
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'admin','2019-05-19 09:47:51','2019-05-19 09:47:51');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_01_01_000000_add_voyager_user_fields',1),(4,'2016_01_01_000000_create_data_types_table',1),(5,'2016_05_19_173453_create_menu_table',1),(6,'2016_10_21_190000_create_roles_table',1),(7,'2016_10_21_190000_create_settings_table',1),(8,'2016_11_30_135954_create_permission_table',1),(9,'2016_11_30_141208_create_permission_role_table',1),(10,'2016_12_26_201236_data_types__add__server_side',1),(11,'2017_01_13_000000_add_route_to_menu_items_table',1),(12,'2017_01_14_005015_create_translations_table',1),(13,'2017_01_15_000000_make_table_name_nullable_in_permissions_table',1),(14,'2017_03_06_000000_add_controller_to_data_types_table',1),(15,'2017_04_21_000000_add_order_to_data_rows_table',1),(16,'2017_07_05_210000_add_policyname_to_data_types_table',1),(17,'2017_08_05_000000_add_group_to_settings_table',1),(18,'2017_11_26_013050_add_user_role_relationship',1),(19,'2017_11_26_015000_create_user_roles_table',1),(20,'2018_03_11_000000_add_user_settings',1),(21,'2018_03_14_000000_add_details_to_data_types_table',1),(22,'2018_03_16_000000_make_settings_value_nullable',1),(23,'2019_04_21_062419_create_order_types',1),(24,'2019_04_22_090530_create_table_orders',1),(25,'2019_04_24_171112_create_table_active_orders',1),(26,'2019_04_25_101257_create_table_log_orders',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_types`
--

DROP TABLE IF EXISTS `order_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `counter` smallint(5) unsigned NOT NULL DEFAULT 0,
  `prefix` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_types_name_unique` (`name`),
  UNIQUE KEY `order_types_prefix_unique` (`prefix`),
  UNIQUE KEY `order_types_alias_unique` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_types`
--

LOCK TABLES `order_types` WRITE;
/*!40000 ALTER TABLE `order_types` DISABLE KEYS */;
INSERT INTO `order_types` VALUES (1,'Общая очередь','default',0,'A','Без предварительного заказа','2019-07-16 15:35:22','2019-08-15 05:16:47'),(2,'Выдача заказов','online',0,'И','Предварительный заказ через интернет','2019-07-16 15:35:22','2019-08-15 04:43:51');
/*!40000 ALTER TABLE `order_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_type_id` bigint(20) unsigned NOT NULL,
  `client_num` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `print_ticket_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_order_type_id_foreign` (`order_type_id`),
  CONSTRAINT `orders_order_type_id_foreign` FOREIGN KEY (`order_type_id`) REFERENCES `order_types` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1),(1,3),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(21,3),(22,1),(22,3),(23,1),(23,3),(24,1),(25,1),(26,1),(27,1),(27,3),(28,1),(28,3),(29,1),(29,3),(30,1),(30,3),(31,1),(31,3),(32,1),(32,3),(33,1),(33,3),(34,1),(34,3),(35,1),(35,3),(36,1),(36,3),(37,1),(37,3),(38,1),(38,3),(39,1),(39,3),(40,1),(40,3),(41,1),(41,3),(42,1),(42,3),(43,1),(43,3),(44,1),(44,3),(45,1),(45,3),(46,1),(46,3);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'browse_admin',NULL,'2019-05-19 09:47:51','2019-05-19 09:47:51'),(2,'browse_bread',NULL,'2019-05-19 09:47:51','2019-05-19 09:47:51'),(3,'browse_database',NULL,'2019-05-19 09:47:51','2019-05-19 09:47:51'),(4,'browse_media',NULL,'2019-05-19 09:47:51','2019-05-19 09:47:51'),(5,'browse_compass',NULL,'2019-05-19 09:47:51','2019-05-19 09:47:51'),(6,'browse_menus','menus','2019-05-19 09:47:51','2019-05-19 09:47:51'),(7,'read_menus','menus','2019-05-19 09:47:51','2019-05-19 09:47:51'),(8,'edit_menus','menus','2019-05-19 09:47:51','2019-05-19 09:47:51'),(9,'add_menus','menus','2019-05-19 09:47:51','2019-05-19 09:47:51'),(10,'delete_menus','menus','2019-05-19 09:47:51','2019-05-19 09:47:51'),(11,'browse_roles','roles','2019-05-19 09:47:51','2019-05-19 09:47:51'),(12,'read_roles','roles','2019-05-19 09:47:51','2019-05-19 09:47:51'),(13,'edit_roles','roles','2019-05-19 09:47:51','2019-05-19 09:47:51'),(14,'add_roles','roles','2019-05-19 09:47:51','2019-05-19 09:47:51'),(15,'delete_roles','roles','2019-05-19 09:47:51','2019-05-19 09:47:51'),(16,'browse_users','users','2019-05-19 09:47:51','2019-05-19 09:47:51'),(17,'read_users','users','2019-05-19 09:47:51','2019-05-19 09:47:51'),(18,'edit_users','users','2019-05-19 09:47:51','2019-05-19 09:47:51'),(19,'add_users','users','2019-05-19 09:47:51','2019-05-19 09:47:51'),(20,'delete_users','users','2019-05-19 09:47:51','2019-05-19 09:47:51'),(21,'browse_settings','settings','2019-05-19 09:47:51','2019-05-19 09:47:51'),(22,'read_settings','settings','2019-05-19 09:47:51','2019-05-19 09:47:51'),(23,'edit_settings','settings','2019-05-19 09:47:51','2019-05-19 09:47:51'),(24,'add_settings','settings','2019-05-19 09:47:51','2019-05-19 09:47:51'),(25,'delete_settings','settings','2019-05-19 09:47:51','2019-05-19 09:47:51'),(26,'browse_hooks',NULL,'2019-05-19 09:47:51','2019-05-19 09:47:51'),(27,'browse_pharmacies','pharmacies','2019-05-19 09:50:20','2019-05-19 09:50:20'),(28,'read_pharmacies','pharmacies','2019-05-19 09:50:20','2019-05-19 09:50:20'),(29,'edit_pharmacies','pharmacies','2019-05-19 09:50:20','2019-05-19 09:50:20'),(30,'add_pharmacies','pharmacies','2019-05-19 09:50:20','2019-05-19 09:50:20'),(31,'delete_pharmacies','pharmacies','2019-05-19 09:50:20','2019-05-19 09:50:20'),(32,'browse_pharmacy_networks','pharmacy_networks','2019-05-19 09:56:07','2019-05-19 09:56:07'),(33,'read_pharmacy_networks','pharmacy_networks','2019-05-19 09:56:07','2019-05-19 09:56:07'),(34,'edit_pharmacy_networks','pharmacy_networks','2019-05-19 09:56:07','2019-05-19 09:56:07'),(35,'add_pharmacy_networks','pharmacy_networks','2019-05-19 09:56:07','2019-05-19 09:56:07'),(36,'delete_pharmacy_networks','pharmacy_networks','2019-05-19 09:56:07','2019-05-19 09:56:07'),(37,'browse_order_types','order_types','2019-05-19 10:03:09','2019-05-19 10:03:09'),(38,'read_order_types','order_types','2019-05-19 10:03:09','2019-05-19 10:03:09'),(39,'edit_order_types','order_types','2019-05-19 10:03:09','2019-05-19 10:03:09'),(40,'add_order_types','order_types','2019-05-19 10:03:09','2019-05-19 10:03:09'),(41,'delete_order_types','order_types','2019-05-19 10:03:09','2019-05-19 10:03:09'),(42,'browse_cashboxes','cashboxes','2019-05-22 06:38:43','2019-05-22 06:38:43'),(43,'read_cashboxes','cashboxes','2019-05-22 06:38:43','2019-05-22 06:38:43'),(44,'edit_cashboxes','cashboxes','2019-05-22 06:38:43','2019-05-22 06:38:43'),(45,'add_cashboxes','cashboxes','2019-05-22 06:38:43','2019-05-22 06:38:43'),(46,'delete_cashboxes','cashboxes','2019-05-22 06:38:43','2019-05-22 06:38:43');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','Administrator','2019-05-19 09:46:52','2019-05-19 09:46:52'),(2,'user','Normal User','2019-05-19 09:47:51','2019-05-19 09:47:51'),(3,'EOrder Admin','Администратор - электронной очереди','2019-05-22 07:03:02','2019-05-22 07:03:02');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'site.title','Site Title','EOrder','','text',1,'Site'),(2,'site.description','Site Description','EOrder Admin Panel','','text',2,'Site'),(3,'site.logo','Site Logo','settings\\August2019\\5qGtnZN4QvXhbZnJLwdI.png','','image',9,'Site'),(5,'admin.bg_image','Admin Background Image','settings\\August2019\\chHHZ3WM3Q4AQOhMuJt9.jpg','','image',5,'Admin'),(6,'admin.title','Admin Title','EOrder','','text',1,'Admin'),(7,'admin.description','Admin Description','EOrder Admin Panel','','text',2,'Admin'),(8,'admin.loader','Admin Loader','','','image',3,'Admin'),(9,'admin.icon_image','Admin Icon Image','','','image',4,'Admin'),(10,'admin.google_analytics_client_id','Google Analytics Client ID (used for admin dashboard)',NULL,'','text',1,'Admin'),(11,'site.terminal_header','Приветствие терминала','Пожалуйста, возьмите талон с номером',NULL,'text',3,'Site'),(12,'site.terminal_contact_header','Терминал контактная информация','Телефон информационно-справочной службы +7 (499) 603-00-00',NULL,'text',6,'Site'),(13,'site.default_terminal_banner','Баннер для терминала по-умолчанию','settings\\August2019\\zhX0cqEve48DNMpziaGk.jpg',NULL,'image',10,'Site'),(14,'site.default_board_banner','Баннер для табло по-умолчанию','settings\\August2019\\B8CaJtkFnmtxDwzEfzx2.png',NULL,'image',11,'Site'),(15,'site.board_eorder_header','Заголовок электронной очереди табло','ЭЛЕКТРОННАЯ ОЧЕРЕДЬ',NULL,'text',7,'Site'),(16,'site.pharmacy_name','Название аптеки','АПТЕКА ОЗЕРКИ',NULL,'text',8,'Site');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translations`
--

LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `user_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'khitrov.av@erkapharm.com','khitrov.av@erkapharm.comm','users/default.png',NULL,'$2y$10$XgSKLx.SPyga6Srn4h6U8uaI0j.lcUqPV4LGwoIRHgWW/ZZP2iwSe','6L9lXXmQHD7VsvbduCT4KhwnSPNcyXjfoJzubn5OGuYeTLC8c66ysBCsJVZF','{\"locale\":\"ru\"}','2019-05-19 09:46:52','2019-05-20 07:53:38'),(3,1,'admin','admin@erkapharm.com','users\\August2019\\pkuw3xMa5Rx5A2gfk6HA.png',NULL,'$2y$10$qvknpd00wU5IB0Yb5RO4a.Pkyqd4wVvdb9yxWOq3vLkpVIyeZLOu2',NULL,'{\"locale\":\"ru\"}','2019-05-20 07:53:57','2019-08-14 06:54:19'),(4,3,'eorder@erkapharm.com','eorder@erkapharm.com','users\\August2019\\HNQ95HLqYrWmRD1KoV9D.png',NULL,'$2y$10$KxFyyCYNMayZPuABSQUTAeuBDer39zLmQWoZJSLi/UrrkH3MW3WFS',NULL,'{\"locale\":\"ru\"}','2019-05-22 07:03:38','2019-08-14 06:28:11');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-15 11:19:41
