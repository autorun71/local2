<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PharmacyNetwork extends Model
{
    public function pharmacies()
    {
        return $this->hasMany('App\Pharmacy');
    }
}
