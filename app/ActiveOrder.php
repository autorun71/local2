<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActiveOrder extends Model
{
    public function cashbox()
    {
        return $this->hasOne('App\Cashbox', 'id', 'cashbox_id');
    }

    public function type() {
        return $this->hasOne('App\OrderType', 'id', 'order_type_id');
    }
}
