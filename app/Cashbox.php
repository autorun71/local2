<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cashbox extends Model
{
    protected $table = 'cashboxes';

    public function orderTypes()
    {
        return $this->belongsToMany('App\OrderType', 'cashbox_order');
    }
}
