<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromoBasket extends Model
{
    protected $fillable = [
        'product_id',
        'name',
        'description',
        'price',
        'count',
        'cost',
    ];
}
