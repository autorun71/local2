<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashboxOrder extends Model
{
    protected $table = 'cashbox_order';
}
