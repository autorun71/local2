<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromoProduct extends Model
{
    protected $fillable = [
        'product_id',
        'name',
        'brand',
        'price',
        'image_url',
    ];
}
