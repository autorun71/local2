<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromoAdditional extends Model
{
    protected $fillable = [
        'product_id',
        'name',
        'description',
        'price',
        'image_url',
    ];
}
