<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PharmacyOrder extends Model
{
    protected $table = 'pharmacy_order';
}
