<?php

namespace App\Traits;

use App\Traits\ApiRequestTrait;
use Log;

trait EcomTrait
{
    use ApiRequestTrait;

    private $ecomUrl;
    private $ecomLogin;
    private $ecomPassword;

    public function ecomGetStatuses()
    {
        $this->ecomSetUrl();
        $url = $this->ecomUrl . '/statuses/orders';
        Log::info("Connect to: " . $url);
        return $this->apiGetRequest($url, $this->ecomLogin, $this->ecomPassword);
    }

    public function ecomGetTimeSlots()
    {
        $this->ecomSetUrl();
        $url = $this->ecomUrl . '/delivery/time_slots';
        Log::info("Connect to: " . $url);
        return $this->apiGetRequest($url, $this->ecomLogin, $this->ecomPassword);
    }

    public function ecomStatusUpdate($orderId, $statusId, $comment = null)
    {
        $this->ecomSetUrl();
        $url = $this->ecomUrl . '/orders/' . $orderId;
        Log::info("Connect to: " . $url);

        $headers = [
            'Accept: application/json',
            'Content-Type: application/json',
        ];

        $data = [
            'statusId' => $statusId,
            'comment' => $comment,
        ];

        return $this->apiPostJsonRequest($url, $data, $headers, $this->ecomLogin, $this->ecomPassword, 'PUT');
    }

    public function ecomGetProduct($productId)
    {
        $this->ecomSetUrl();
        $url = $this->ecomUrl . '/goods/' . $productId;
        Log::info("Connect to ECOM: " . $url);
        return $this->apiGetRequest($url, $this->ecomLogin, $this->ecomPassword);
    }

    private function ecomSetUrl()
    {
        $this->ecomUrl = config('app.ecom_url');
        $this->ecomLogin = config('app.ecom_login');
        $this->ecomPassword = config('app.ecom_password');
    }
}
