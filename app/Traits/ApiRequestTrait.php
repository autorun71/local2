<?php

namespace App\Traits;

trait ApiRequestTrait
{
    private function apiPostJsonRequest($url, $data = [], $headers = [], $username = null, $password = null, $method = 'POST')
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLINFO_HEADER_OUT, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);

        if (count($data) > 0) {
            $json = json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
        }

        $result = curl_exec($curl);
        if ($result === false) {
            throw new \Exception(curl_error($curl), curl_errno($curl));
        }
        return $result;
    }

    private function apiGetRequest($url, $username = null, $password = null)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        if (!is_null($username) && !is_null($password)) {
            curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password);
        }

        $result = curl_exec($curl);
        if ($result === false) {
            throw new \Exception(curl_error($curl), curl_errno($curl));
        }
        return $result;
    }

    private function apiPostRequest($url, $data = [], $headers = [], $username = null, $password = null)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLINFO_HEADER_OUT, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        if (!is_null($username) && !is_null($password)) {
            curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password);
        }

        if (count($data) > 0) {
            $json = json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        }

        $result = curl_exec($curl);
        if ($result === false) {
            throw new \Exception(curl_error($curl), curl_errno($curl));
        }
        return $result;
    }

    private function apiUrlEncodedRequest($url, $data = [], $headers = [])
    {
        $curl = curl_init();

        $uri = '';
        foreach ($data as $array) {
            foreach ($array as $key => $value) {
                $uri .= $key . '=' . urlencode($value) . '&';
            }
        }

        curl_setopt($curl, CURLOPT_URL, $url . '?' . $uri);

        if (count($headers) > 0) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        }

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($curl);
        if ($result === false) {
            throw new \Exception(curl_error($curl), curl_errno($curl));
        }
        return $result;
    }

    private function apiPostUrlEncodedRequest($url, $data = [], $headers = [])
    {
        $curl = curl_init();

        $uri = '';
        foreach ($data as $name => $value) {
            $uri .= urlencode($name) . '=' . urlencode($value) . '&';
        }

        curl_setopt($curl, CURLOPT_URL, $url . '/' . $uri);

        if (count($headers) > 0) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        }

        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $uri);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');

        $result = curl_exec($curl);
        if ($result === false) {
            throw new \Exception(curl_error($curl), curl_errno($curl));
        }
        return $result;
    }

    private function apiGetUrlEncodedRequest($url, $data = [], $headers = [])
    {
        $curl = curl_init();

        $uri = '';
        foreach ($data as $name => $value) {
            $uri .= urlencode($name) . '=' . urlencode($value) . '&';
        }

        curl_setopt($curl, CURLOPT_URL, $url . '/' . $uri);

        if (count($headers) > 0) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        }

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($curl);
        if ($result === false) {
            throw new \Exception(curl_error($curl), curl_errno($curl));
        }
        return $result;
    }

    private function apiPostXmlRequest($url, $xmlRequest, $headers = [])
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);

        if (count($headers) > 0) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        }

        if (!empty($xmlRequest)) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, 'xml_request=' . $xmlRequest);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        }

        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($curl);
        if ($result === false) {
            throw new \Exception(curl_error($curl), curl_errno($curl));
        }
        return $result;
    }
}
