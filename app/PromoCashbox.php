<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromoCashbox extends Model
{
    protected $fillable = [
        'cashbox_id',
        'basket_cost',
        'active'
    ];

    public function basket()
    {
        return $this->hasMany('App\PromoBasket');
    }

    public function additional()
    {
        return $this->hasMany('App\PromoAdditional');
    }

    public function product()
    {
        return $this->hasOne('App\PromoProduct');
    }
}
