<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromoProductImage extends Model
{
    protected $fillable = [
        'product_id',
        'url',
        'size',
        'modify_time',
    ];
}
