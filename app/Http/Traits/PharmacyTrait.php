<?php

namespace App\Http\Traits;

use App\Pharmacy;

trait PharmacyTrait
{
    private function getPharmacyInfo()
    {
        $pharmacy = Pharmacy::with('pharmacyNetwork')->where('token_id', $this->token_id)->first();

        return [
            'pharmacy_token_id' => encrypt($this->token_id),
            'pharmacy_view_name' => $pharmacy->view_name ?? $pharmacy->pharmacyNetwork->view_name,
            'pharmacy_telephone' => $pharmacy->telephone ?? $pharmacy->pharmacyNetwork->telephone,
            'pharmacy_logo' => $pharmacy->pharmacyNetwork->logo,
            'pharmacy_banner_terminal' => $pharmacy->banner_terminal ?? $pharmacy->pharmacyNetwork->banner_terminal ?? setting('site.default_terminal_banner'),
            'pharmacy_banner_board' => $pharmacy->banner_board ?? $pharmacy->pharmacyNetwork->banner_board ?? setting('site.default_board_banner')
        ];
    }
}
