<?php

namespace App\Http\Traits;

use App\OrderType;
use App\Pharmacy;

trait RequestTrait
{

    private $tokenId;
    private $cashboxId;
    private $orderAlias;
    private $pharmacy;

    private function getVars($request)
    {
        $requestArray = $request->json()->all();

        $this->tokenId = $requestArray['message']['token_id'];

        if (isset($requestArray['message']['cashbox_id'])) {
            $this->cashboxId = $requestArray['message']['cashbox_id'];
        }

        $orderAlias = null;

        if (isset($requestArray['message']['order_type'])) {
            $this->orderAlias = $requestArray['message']['order_type'];
        }

        $this->pharmacy = Pharmacy::where('token_id', $this->tokenId)->first();
        if (empty($this->pharmacy->id)) {
            return abort(401);
        }
    }
}
