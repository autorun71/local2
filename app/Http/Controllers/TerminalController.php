<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Traits\PharmacyTrait;
use App\OrderType;
use App\PharmacyOrder;
use App\Pharmacy;
use App\Order;
use DB;
use Log;

class TerminalController extends Controller
{

    public function device(Request $request)
    {
        return view('device.terminal.terminal_index');
    }

    public function returnUrl(Request $request)
    {

        if (isset($request->defaultOrderButton)) {
            $order_type = 'default';
        } elseif (isset($request->onlineOrderButton)) {
            $order_type = 'online';
        } else {
            return redirect('/device/terminal/error/', 301);
        }
        return redirect('/device/terminal/success/' . $order_type . '/', 301);
    }
}
