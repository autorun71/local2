<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PromoController extends Controller
{
    public function index() {
        return view('promo');
    }

    public function indexPromo(Request $request) {
        return view('promo', [
            'axis' => $request->axis,
            'cashbox_id' => $request->cashbox_id,
        ]);
    }

    public function show($cashbox_id) {
        return view('promo', [
            'cashbox_id' => $cashbox_id,
        ]);
    }
}
