<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\ActiveOrder;
use App\LogOrder;
use App\Cashbox;
use DB;
use App\Http\Traits\RequestTrait;

class CashboxController extends Controller
{

    public function index(Request $request)
    {
        return view('cashboxes');
    }
}
