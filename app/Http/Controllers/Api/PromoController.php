<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PromoCashbox;
use App\Traits\EcomTrait;
use App\PromoProductImage;
use Illuminate\Support\Facades\Storage;
use Log;

class PromoController extends Controller
{
    use EcomTrait;

    public function show($cashboxId)
    {
        try {
            $this->log('Касса ' . $cashboxId . ' получение данных');
            $promo = PromoCashbox::where('cashbox_id', $cashboxId)->first();
            if (!blank($promo)) {
                $this->responseArray = [
                    'basket' => $promo->basket()->get(),
                    'additional' => $promo->additional()->get(),
                    'basket_cost' => $promo->basket_cost,
                    'product' => $promo->product()->first(),
                    'show_promo' => false,
                ];
                $this->log('Касса ' . $cashboxId . ' сессия открыта данные успешно получены');
                return $this->output();
            } else {
                $this->responseArray = [
                    'message' => 'Объект не найден',
                    'show_promo' => true,
                ];
                $this->log('Касса ' . $cashboxId . ' отображение рекламы данные успешно получены');
                return $this->output();
            }
        } catch (\Exception $e) {
            $this->log('Касса ' . $cashboxId . ' ошибка получения данных');
            throw new \Exception($e->getMessage());
        }
    }

    public function store(Request $request)
    {
        try {
            $basket = $request->input('basket');
            $additional = $request->input('additional');
            $cashboxId = $request->input('cashbox_id');
            $basketCost = $request->input('basket_cost');
            $product = $request->input('product');

            $promo = PromoCashbox::updateOrCreate(
                ['cashbox_id' => $cashboxId],
                ['basket_cost' => $basketCost],
            );

            //Clean basket
            if (!blank($promo) && blank($basket) && blank($product)) {
                $promo->basket()->delete();
                $this->log('Касса ' . $request->cashbox_id . ' корзина очищена запрос: ' . $request->getContent());
            }

            //Add to basket
            if (!blank($basket)) {

                //Clean basket
                $promo->basket()->delete();
                $promo->product()->delete();

                foreach ($basket as $item) {
                    $promo->basket()->create($item);
                }
                $this->log('Касса ' . $request->cashbox_id . ' добавлены товары в корзину запрос: ' . $request->getContent());
            }

            //Add to additional
            //@MAY BE Delete all and add new JSON array
            if (!blank($additional)) {

                //Clean additional
                $promo->additional()->delete();
                $promo->product()->delete();

                foreach ($additional as $item) {
                    $promo->additional()->updateOrCreate(
                        [
                            'product_id' => $item['product_id'],
                        ],
                        [
                            'name' => $item['name'],
                            'price' => $item['price'],
                            'image_url' => $this->getProductImageUrl($item['product_id']),
                        ]
                    );
                }
                $this->log('Касса ' . $request->cashbox_id . ' добавлены доп. товары запрос: ' . $request->getContent());
            }

            //Add to product
            if (!blank($product)) {
                $promo->product()->delete();
                $promo->product()->updateOrCreate(
                    [
                        'product_id' => $product['product_id'],
                    ],
                    [
                        'name' => $product['name'],
                        'price' => $product['price'],
                        'brand' => $product['brand'],
                        'image_url' => $this->getProductImageUrl($product['product_id']),
                    ]
                );
                $this->log('Касса ' . $request->cashbox_id . ' добавлен основной продукт запрос: ' . $request->getContent());
            }

            $this->responseArray = [
                'message' => 'Данные успешно добавлены/обновлены'
            ];
            return $this->output();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function destroy(Request $request)
    {
        try {
            $basket = $request->input('basket');
            $additional = $request->input('additional');
            $cashboxId = $request->input('cashbox_id');

            $promo = PromoCashbox::where('cashbox_id', $cashboxId)->first();

            //Delete from basket
            if (!blank($promo) && !blank($basket)) {
                foreach ($basket as $item) {
                    $product = $promo->basket()->where('product_id', $item['product_id'])->first();
                    if (!blank($product)) {
                        $product->delete();
                    }
                }
            }

            //Delete from additional
            //@MAY BE Delete all and add new JSON array
            if (!blank($promo) && !blank($additional)) {
                foreach ($additional as $item) {
                    $product = $promo->additional()->where('product_id', $item['product_id'])->first();
                    if (!blank($product)) {
                        $product->delete();
                    }
                }
            }

            $this->responseArray = [
                'message' => 'Данные успешно удалены'
            ];

            return $this->output();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function addProductToBasket(Request $request)
    {
        try {
            $productId = $request->input('product_id');
            if (!blank($productId)) {
                $this->responseArray = [
                    'message' => 'Товар успешно добавлен в корзину'
                ];
                return $this->output();
            } else {
                throw new \Exception('Ошибка добавления товара в корзину');
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function deleteProduct($cashboxId)
    {
        try {
            $promo = PromoCashbox::where('cashbox_id', $cashboxId)->first();
            if (!blank($promo) && $promo->product()->delete()) {
                $this->responseArray = [
                    'message' => 'Продукт успешно удалён',
                ];
            } else {
                throw new \Exception('Ошибка удаления продукта');
            }
            return $this->output();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function clearBasket($cashboxId)
    {
        try {
            $promo = PromoCashbox::where('cashbox_id', $cashboxId)->first();
            if (!blank($promo) && $promo->basket()->delete()) {
                $promo->basket_cost = 0;
                $promo->save();
                $this->responseArray = [
                    'message' => 'Корзина успешно очищена',
                ];
            } else {
                throw new \Exception('Ошибка очистки корзины');
            }
            return $this->output();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function runSession($cashboxId)
    {
        try {
            if (PromoCashbox::updateOrCreate(
                ['cashbox_id' => $cashboxId],
            )) {
                $this->responseArray = [
                    'message' => 'Сессия успешно открыта',
                ];
            } else {
                throw new \Exception('Ошибка открытия сессии');
            }
            return $this->output();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function closeSession($cashboxId)
    {
        try {
            if (PromoCashbox::where('cashbox_id', $cashboxId)->delete()) {
                $this->responseArray = [
                    'message' => 'Сессия успешно закрыта',
                ];
                $this->log('Касса ' . $cashboxId . ' сессия успешно закрыта');
            } else {
                $this->log('Касса ' . $cashboxId . ' ошибка закрытия сессии', 'err');
                throw new \Exception('Ошибка закрытия сессии');
            }
            return $this->output();
        } catch (\Exception $e) {
            $this->log('Касса ' . $cashboxId . ' ошибка закрытия сессии(исключение)', 'err');
            throw new \Exception($e->getMessage());
        }
    }

    public function getProductImageUrl($productId)
    {
        //Ecom request
        $result = json_decode($this->ecomGetProduct($productId), true);

        //Check if ftp image URL exist
        if (count($result['goods']) > 0 && array_key_exists('imgLinkFTP', $result['goods'][0])) {
            $url = $result['goods'][0]['imgLinkFTP'];

            $ftpFile = parse_url($url, PHP_URL_PATH);
            $ftpFileName = pathinfo($ftpFile);
            $ftpFileBaseName = $ftpFileName['basename'];

            //Get image from model
            $image = PromoProductImage::where('product_id', $productId)->first();

            if (blank($image)) {

                //Get file and metadata from FTP
                $file = Storage::disk('ftp')->get($ftpFile);
                $modifyTime = Storage::disk('ftp')->lastModified($ftpFile);

                //Save file to local storage and model
                if (Storage::disk('productImages')->put($ftpFileBaseName, $file)) {
                    $size = Storage::disk('productImages')->size($ftpFileBaseName);
                    $image = PromoProductImage::updateOrCreate(
                        [
                            'product_id' => $productId
                        ],
                        [
                            'url' => $ftpFileBaseName,
                            'size' => $size,
                            'modify_time' => $modifyTime,
                        ],
                    );
                }
            }
            return url('/') . '/images/product/' . $image->url;
        }
        //Return default images URL
        return url('/') . '/images/product/default.png';
    }

    private function output($code = 200)
    {
        return response()->json(array_merge(
            [
                'success' => true,
                'httpCode' => $code,
            ],
            $this->responseArray
        ), $code, [], JSON_UNESCAPED_UNICODE);
    }

    private function log($message, $severety = 'info')
    {
        if (config('app.debug')) {
            if ($severety === 'info') {
                Log::info($message);
            } elseif ($severety === 'warn') {
                Log::warning($message);
            } elseif ($severety === 'err') {
                Log::error($message);
            }
        }
    }
}
