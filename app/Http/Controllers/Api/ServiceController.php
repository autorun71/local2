<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\OrderType;
use App\Http\Controllers\Controller;

class ServiceController extends Controller
{
    public function reset() {
        $orders = OrderType::all();
        foreach($orders as $order) {
            $order->counter = 0;
            $order->save();
        }
    }
}
