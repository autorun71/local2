<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Terminal
Route::post('/terminal', 'Api\TerminalController@getOrderNum');

//Cashbox
Route::post('/cashbox/set', 'Api\CashboxController@setCacheRegister');
Route::post('/cashbox/next', 'Api\CashboxController@getNextClient');
Route::post('/cashbox/next/id', 'Api\CashboxController@getNextClientById');
Route::post('/cashbox/repeat', 'Api\CashboxController@repeatNextClient');
Route::post('/cashbox/return', 'Api\CashboxController@returnClient');
Route::post('/cashbox/stop', 'Api\CashboxController@stopCurrentClient');
Route::post('/cashbox/remove', 'Api\CashboxController@removeClient');
Route::post('/cashbox/current', 'Api\CashboxController@getCurrentClient');
Route::get('/cashbox/list', 'Api\CashboxController@getList');

//Board
Route::post('/board/active', 'Api\BoardController@getActiveListApi');
Route::post('/tv', 'Api\BoardController@getActiveListApi');
Route::post('/board/active/call', 'Api\BoardController@getSoundClient');
Route::get('/board/active/disable/{id}', 'Api\BoardController@disableSoundClient');

//Counters
Route::get('/reset-counters', 'Api\ServiceController@reset');

//Promo
Route::apiResource('/promo', 'Api\PromoController');
Route::delete('/promo', 'Api\PromoController@destroy');
Route::put('/promo', 'Api\PromoController@addProductToBasket');
Route::get('/promo/delete-product/{cashbox_id}', 'Api\PromoController@deleteProduct');
Route::get('/promo/clear-basket/{cashbox_id}', 'Api\PromoController@clearBasket');
Route::get('/promo/run-session/{cashbox_id}', 'Api\PromoController@runSession');
Route::get('/promo/close-session/{cashbox_id}', 'Api\PromoController@closeSession');
