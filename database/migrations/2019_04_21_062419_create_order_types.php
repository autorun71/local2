<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 256)->unique();
            $table->string('alias', 128)->nullable($value = true)->unique();
            $table->unsignedSmallInteger('counter')->default(0);
            $table->string('prefix', 16)->unique();
            $table->string('comment', 256)->nullable($value = true);
            $table->timestampsTz();

            $table->engine = 'InnoDB';
        });

        //Add default values
        DB::table('order_types')->insert(
            array(
                [
                    'name' => 'Основная очередь',
                    'prefix' => 'A',
                    'alias' => 'default',
                    'comment' => 'Без предварительного заказа',
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now()
                ],
                [
                    'name' => 'Интернет-очередь',
                    'prefix' => 'И',
                    'alias' => 'online',
                    'comment' => 'Предварительный заказ через интернет',
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now()
                ]
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_types');
    }
}
