<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromoAdditionalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promo_additionals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('promo_cashbox_id');
            $table->string('product_id');
            $table->string('name');
            $table->decimal('price', 8, 2);
            $table->string('image_url');
            $table->timestampsTz();

            $table->foreign('promo_cashbox_id')
                ->references('id')->on('promo_cashboxes')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promo_additionals');
    }
}
