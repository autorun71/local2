<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromoCashboxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promo_cashboxes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cashbox_id')->index()->unique();
            $table->decimal('basket_cost', 10, 2)->nullable();
            $table->boolean('active')->default(true);
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promo_hashes');
    }
}
