<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableActiveOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('active_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_type_id');
            $table->string('client_num', 32);
            $table->boolean('sound_call');
            $table->unsignedBigInteger('cashbox_num')->unique();
            $table->timestamp('start_time')->nullable($value = true);
            $table->timestamp('print_ticket_time')->nullable($value = true);
            $table->timestamp('active_time')->nullable($value = true);
            $table->timestampsTz();

            //Foreign Keys
            $table->foreign('order_type_id')
                ->references('id')->on('order_types')
                ->onDelete('cascade');

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('active_orders');
    }
}
