<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromoProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promo_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('promo_cashbox_id')->unique();
            $table->string('product_id')->unique();
            $table->string('name');
            $table->string('brand')->nullable();
            $table->decimal('price', 8, 2);
            $table->string('image_url');
            $table->timestampsTz();

            $table->foreign('promo_cashbox_id')
                ->references('id')->on('promo_cashboxes')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promo_products');
    }
}
