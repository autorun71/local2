<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromoBasketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promo_baskets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('promo_cashbox_id');
            $table->string('product_id');
            $table->string('name');
            $table->unsignedInteger('count')->default(0);
            $table->decimal('price', 8, 2);
            $table->decimal('cost', 8, 2);
            $table->timestampsTz();

            $table->foreign('promo_cashbox_id')
                ->references('id')->on('promo_cashboxes')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promo_carts');
    }
}
