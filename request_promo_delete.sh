#!/bin/bash

#URL=http://eorder-dev.test.erkapharm.ru/~khitrovav/api/promo
URL=http://eorder.test.erkapharm.ru/api/promo

curl -s -X DELETE \
--header 'Content-Type: application/json' \
--data '{
    "cashbox_id": "1",
    "basket": [
        {
            "product_id": 91989
        }
    ],
    "additional": [
        {
            "product_id": 91989
        }
]}' $URL

exit 0
