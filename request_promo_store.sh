#!/bin/bash

URL=http://eorder-dev.test.erkapharm.ru/~khitrovav/api/promo
#URL=http://eorder.test.erkapharm.ru/api/promo
URL=http://10.122.39.87/api/promo

curl -s -X POST \
--header 'Content-Type: application/json' \
--data "{
    \"cashbox_id\": $1,
    \"basket\": [
        {
            \"product_id\": 91989,
            \"name\": \"Авент Нейчерал Соска силиконовая средний поток с 3 мес №2 (арт. 80530)\",
            \"price\": 83.00,
            \"count\": 5,
            \"cost\": 83.00
        },
        {
            \"product_id\": 91988,
            \"name\": \"Авент Нейчерал Соска силиконовая средний поток с 3 мес №2 (арт. 80530)\",
            \"price\": 83.00,
            \"count\": 1,
            \"cost\": 83.00
        },
        {
            \"product_id\": 91987,
            \"name\": \"Авент Нейчерал Соска силиконовая средний поток\",
            \"price\": 83.00,
            \"count\": 5,
            \"cost\": 83.00
        },
        {
            \"product_id\": 91986,
            \"name\": \"Авент Нейчерал Соска силиконовая средний потокkkkkkkkkkkkkkkkkkkkkkkk kkkkkkkkkkkkkkkkkkkkkkk kkkkkkkkkkkkkkk\",
            \"price\": 99000.00,
            \"count\": 1,
            \"cost\": 99000.00
        },
        {
            \"product_id\": 91981,
            \"name\": \"Авент Нейчерал Соска силиконовая средний потокkkkkkkkkkkkkkkkkkkkkkkk kkkkkkkkkkkkkkkkkkkkkkk kkkkkkkkkkkkkkk\",
            \"price\": 99000.00,
            \"count\": 1,
            \"cost\": 99000.00
        },
        {
            \"product_id\": 91966,
            \"name\": \"Авент Нейчерал Соска силиконовая средний потокkkkkkkkkkkkkkkkkkkkkkkk kkkkkkkkkkkkkkkkkkkkkkk kkkkkkkkkkkkkkk\",
            \"price\": 99000.00,
            \"count\": 1,
            \"cost\": 99000.00
        }
    ],
    \"basket_cost\": 19876.00
}" $URL

exit 0
