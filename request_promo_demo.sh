#!/bin/bash

TIMEOUT=1
URL=http://eorder-dev.test.erkapharm.ru/~khitrovav/api/promo
#URL=http://eorder.test.erkapharm.ru/api/promo
#URL=http://10.122.39.87/api/promo
URL=http://10.41.20.91/api/promo

add_product() {
    echo "Add product"
    curl -s -X POST \
    -H 'Content-Type: application/json' \
    -d "{
            \"cashbox_id\": $1,
            \"basket\": [
                            {
                                \"product_id\": $2,
                                \"name\": \"$3\",
                                \"price\": $4,
                                \"count\": $5,
                                \"cost\": $6
                            }
            ],
            \"basket_cost\": $7
    }" $URL | jq
    sleep $TIMEOUT
}

echo "Add additional products"
curl -s -X POST \
-H 'Content-Type: application/json' \
-d "{
    \"cashbox_id\": $1,
    \"additional\": [
        {
            \"product_id\": 912,
            \"name\": \"КОДАЛИ Гель для душа и ванн 200 мл. арт.016\",
            \"price\": 765.00
        },
        {
            \"product_id\": 942,
            \"name\": \"КОДАЛИ Эмульсия Пульп Витамине от 1-ых морщин 40мл арт.019\",
            \"price\": 786.00
        },
        {
            \"product_id\": 929,
            \"name\": \"КОДАЛИ Винэксперт сыворотка укрепляющая 30мл\",
            \"price\": 786.00
        },
        {
            \"product_id\": 5,
            \"name\": \"Dirol резинка жевательная Морозная Мята 13,6г №10\",
            \"price\": 786.00
        },
        {
            \"product_id\": 392,
            \"name\": \"Иммуноглобулин человеческий для в/в применения 5% фл. 25мл N1\",
            \"price\": 786.00
        },
        {
            \"product_id\": 871,
            \"name\": \"Ессентуки-17 Минеральная вода 0,5л бутылка\",
            \"price\": 786.00
        },
        {
            \"product_id\": 872,
            \"name\": \"Ессентуки-4 Минеральная вода 0,5л бутылка\",
            \"price\": 786.00
        },
        {
            \"product_id\": 662,
            \"name\": \"Полисорб МП пор 1г пак N10\",
            \"price\": 786.00
        },
        {
            \"product_id\": 494,
            \"name\": \"Омник Окас таб. 0.4 мг. №30\",
            \"price\": 786.00
        }
    ],
    \"basket_cost\": 0.00
}" $URL | jq
sleep $TIMEOUT

echo "Add main product"
curl -s -X POST \
-H 'Content-Type: application/json' \
-d "{
        \"cashbox_id\": $1,
        \"product\": {
            \"product_id\": 975,
            \"brand\": \"Лендацин\",
            \"name\": \"Лендацин пор. д/р-ра для в/в и в/м введ. 1000 мг. фл. №5\",
            \"price\": 156.00
        },
        \"basket_cost\": 0.00
}" $URL | jq
sleep $TIMEOUT

echo "Remove main product"
curl -s -X GET $URL/delete-product/$1 | jq
sleep $TIMEOUT

add_product $1 362 "А-Дерма Реальба Мыло без мыла с молочком овса дерматологическое 100г" 80.00 2 160.00 160.00
add_product $1 572 "Спеленок Сок яблоко зеленое обогащенный железом 0,2л" 70.00 2 140.00 300.00
add_product $1 555 "Флорацид таб. п.о. 250 мг. №5" 70.00 2 140.00 400.00
add_product $1 393 "Релаксан Гольфы компрессионные CottonSocks 140den мужские черные р.4 (820)" 70.00 2 140.00 450.00
add_product $1 546 "Пропафенон таб. п.о. 150 мг. №40" 70.00 2 140.00 470.00
add_product $1 368 "Пластырь Omnipor фиксир гипоаллергенный 5см х 5м №1" 70.00 2 140.00 480.00
add_product $1 975 "Лендацин пор. д/р-ра для в/в и в/м введ. 1000 мг. фл. №5" 70.00 2 140.00 490.00
add_product $1 399 "Линза контактная Biomedics 38 R=8,6  -0,75" 70.00 2 140.00 510.00
add_product $1 484 "Сальгим р-р д/ингал. 10 мл. фл. №10" 70.00 2 140.00 550.00
add_product $1 338 "Бандаж хирург. послеоперац. на брюшную стенку (3-х панельный, шир. 23 см.) AB-309 разм. S" 70.00 2 140.00 678.00
add_product $1 544 "Кораксан таб.п.п.о.5мг №56" 70.00 2 140.00 765.00
add_product $1 558 "Кестин таб.п.п.о.20мг №10" 70.00 2 140.00 890.00
add_product $1 746 "Отривин спрей наз.доз.0,1% фл.10мл" 70.00 2 140.00 990.00
add_product $1 485 "Жанин таб.п.о.2мг+0,03мг №63" 70.00 2 140.00 1990.00
add_product $1 858 "Каталин табл. д/глазн капель + р-ль фл 15мл N1" 70.00 2 140.00 2990.00
add_product $1 549 "Акридерм ГК крем д/наруж.прим.0,05%+0,1%+1% туба 30г" 70.00 2 140.00 3450.00
add_product $1 358 "БейбиКалм сироп д/младенцев фл с дозатором 15мл №1" 70.00 2 140.00 4356.00
add_product $1 34 "Мустела Бебе Гель д/купания с рождения фл.с помпой 500мл №1 арт.8301708" 70.00 2 140.00 5678.00
add_product $1 487 "Аугментин таб.п.п.о.500мг+125мг №14" 70.00 2 140.00 6980.00
add_product $1 719 "Нафтадерм 10% 35г" 70.00 2 140.00 7564.00
add_product $1 489 "Итразол 100мг капс №6" 70.00 2 140.00 8674.00

echo "Clear basket"
curl -s -X GET $URL/clear-basket/$1 | jq
sleep $TIMEOUT

echo "Close session"
curl -s -X GET $URL/close-session/$1 | jq

exit 0
