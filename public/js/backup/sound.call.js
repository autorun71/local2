/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/sound.call.js":
/*!************************************!*\
  !*** ./resources/js/sound.call.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function play(audio) {
  var playPromise = audio.play();

  if (playPromise !== undefined) {
    playPromise.then(function (_) {//console.log('Play audio');
    })["catch"](function (error) {
      //Fix Promise exception in Chrome
      console.log('Promise exception');
      setTimeout(function () {
        window.location.reload(true);
      }, 5000);
    });
  }

  return new Promise(function (resolve, reject) {
    audio.addEventListener('ended', resolve);
  });
}

$(document).ready(function () {
  //var token_id = Cookies.get('token_id');
  //var dataToSend = {message: {"token_id": token_id, "order_type": "default"}};
  //Set default delay flag and fix overlay
  var delay = 0;
  var audioFiles = ["/storage/sounds/bell.mp3", "/storage/sounds/client.mp3", "/storage/sounds/a.mp3", "/storage/sounds/i.mp3", "/storage/sounds/1.mp3", "/storage/sounds/2.mp3", "/storage/sounds/go.mp3"];

  function preloadAudio(url) {
    var audio = new Audio();
    audio.addEventListener('canplaythrough', function () {
      console.log('Finish:' + url);
    }, false);
    audio.src = url;
  }

  for (var i in audioFiles) {
    preloadAudio(audioFiles[i]);
  }

  (function update() {
    $.ajax({
      type: 'POST',
      url: "/api/board/active/call",
      //data: JSON.stringify(dataToSend),
      success: function success(data) {
        var json = JSON.parse(data);

        if (json !== null && delay == 0) {
          var callClient = function callClient() {
            var audio1 = new Audio('/storage/sounds/bell.mp3');
            var audio2 = new Audio('/storage/sounds/client.mp3');
            var audio3 = new Audio('/storage/sounds/' + orderLetterEn + '.mp3');
            var audio4 = new Audio('/storage/sounds/' + orderNum + '.mp3');
            var audio5 = new Audio('/storage/sounds/go.mp3');
            var audio6 = new Audio('/storage/sounds/' + json.cashbox_num + '.mp3'); //$('body').html('<h1 class="blink">' + json.client_num + ' подойдите к окну номер ' + cashboxNum + '</h1>');

            $('#call-message').removeAttr('hidden');
            $('#client-num').text(json.client_num);
            $('#cashbox-num').text(json.cashbox_num);
            $('#main-content').addClass('opacity-call');
            delay = 1;
            play(audio1).then(function () {
              return play(audio2);
            }).then(function () {
              return play(audio3);
            }).then(function () {
              return play(audio4);
            }).then(function () {
              return play(audio5);
            }).then(function () {
              //$('body').html('<h1>Stop</h1>');
              return play(audio6);
            }).then(function () {
              //$('body').html('<h1>Finish</h1>');
              $('#main-content').removeClass('opacity-call');
              $('#call-message').attr('hidden', true);
              delay = 0;
            });
          }; //Play call sound


          //Format client num
          var orderLetter = json.client_num.substr(0, 1);

          if (orderLetter == 'A') {
            orderLetterEn = 'a';
          } else if (orderLetter == 'И') {
            orderLetterEn = 'i';
          }

          var orderNum = json.client_num.substr(1, 3);
          orderNum = parseInt(orderNum, 10);
          console.log(orderLetter);
          console.log(orderNum);
          callClient(); //Disable sound_call field in DB

          $.ajax({
            type: 'GET',
            url: "/api/board/active/disable/" + json.id
          });
        }
      }
    }).then(function () {
      setTimeout(update, 2000);
    });
  })();
});

/***/ }),

/***/ 3:
/*!******************************************!*\
  !*** multi ./resources/js/sound.call.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/www-01/resources/js/sound.call.js */"./resources/js/sound.call.js");


/***/ })

/******/ });