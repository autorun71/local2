const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.react('resources/js/app.js', 'public/js')
    .js('resources/js/promo/app.js', 'public/js/promo')
    .react('resources/js/promo/app-react.js', 'public/js/promo')
    .sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/promo/app.scss', 'public/css/promo')
    .copy('resources/js/board.js', 'public/js')
    .copy('resources/js/clock.js', 'public/js')
    .copy('resources/sass/board.css', 'public/css')
    .js('resources/js/main.js', 'public/js');