#!/bin/bash

#URL=http://eorder-dev.test.erkapharm.ru/~khitrovav/api/promo
URL=http://eorder.test.erkapharm.ru/api/promo
#URL=http://10.122.39.87/api/promo
URL=http://10.41.20.91/api/promo

curl -s -X POST \
--header 'Content-Type: application/json' \
--data "{
    \"cashbox_id\": $1,
    \"basket\": [
        {
            \"product_id\": 91989,
            \"name\": \"Авент Нейчерал Соска силиконовая средний поток с 3 мес №2\",
            \"price\": 83.00,
            \"count\": 1,
            \"cost\": 83.00
        },
        {
            \"product_id\": 91988,
            \"name\": \"Авент Нейчерал Соска силиконовая средний поток с 3 мес №2\",
            \"price\": 83.00,
            \"count\": 1,
            \"cost\": 83.00
        },
        {
            \"product_id\": 91987,
            \"name\": \"Авент Нейчерал Соска силиконовая средний поток с 3 мес №2\",
            \"price\": 83.00,
            \"count\": 1,
            \"cost\": 83.00
        },
        {
            \"product_id\": 91986,
            \"name\": \"Авент Нейчерал Соска силиконовая средний поток с 3 мес №2\",
            \"price\": 83.00,
            \"count\": 1,
            \"cost\": 83.00
        }
    ],
    \"basket_cost\": 19876.00,
    \"additional\": [
        {
            \"product_id\": 91678,
            \"name\": \"Хаггис Подгузники-трусики для мальчиков размер 4 №17\",
            \"price\": 786.00
        },
        {
            \"product_id\": 91895,
            \"name\": \"Паранит Шампунь-кондиционер уход после обработки 100мл\",
            \"price\": 156.00
        },
        {
            \"product_id\": 91673,
            \"name\": \"Хаггис Подгузники-трусики для мальчиков размер 4 №17\",
            \"price\": 786.00
        },
        {
            \"product_id\": 91896,
            \"name\": \"Паранит Шампунь-кондиционер уход после обработки 100мл\",
            \"price\": 156.00
        },
        {
            \"product_id\": 91679,
            \"name\": \"Хаггис Подгузники-трусики для мальчиков размер 4 №17\",
            \"price\": 786.00
        },
        {
            \"product_id\": 91894,
            \"name\": \"Паранит Шампунь-кондиционер уход после обработки 100мл\",
            \"price\": 156.00
        }
    ]
}" $URL

exit 0
